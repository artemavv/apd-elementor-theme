
// basic countdown timer
function initCountdownTimer( target, endDateString ) {

	var endDate = new Date(endDateString).getTime();
	
	const c_second = 1000;
	const c_minute = c_second * 60;
	const c_hour = c_minute * 60;
	const c_day = c_hour * 24;

	var countDownTimer = setInterval(
		() => {
			let t_now = new Date().getTime();
			let remainingTime = endDate - t_now;

			let daysLeft = Math.trunc(remainingTime / c_day);
			let hoursLeft = Math.trunc((remainingTime % c_day) / c_hour);
			let minutesLeft = Math.trunc((remainingTime % c_hour) / c_minute);
			let secondsLeft = Math.trunc((remainingTime % c_minute) / c_second);

			if ( document.querySelector('.countdown-' + target ) ) {
				if (remainingTime <= 0) {
					document.querySelector('.countdown-' + target + ' .days').innerHTML = '--';
					document.querySelector('.countdown-' + target + ' .hours').innerHTML = '--';
					document.querySelector('.countdown-' + target + ' .minutes').innerHTML = '--';
					document.querySelector('.countdown-' + target + ' .seconds').innerHTML = '--';
				}
				else {
					document.querySelector('.countdown-' + target + ' .days').innerHTML = String(daysLeft).padStart(2, '0');
					document.querySelector('.countdown-' + target + ' .hours').innerHTML = String(hoursLeft).padStart(2, '0');
					document.querySelector('.countdown-' + target + ' .minutes').innerHTML = String(minutesLeft).padStart(2, '0');
					document.querySelector('.countdown-' + target + ' .seconds').innerHTML = String(secondsLeft).padStart(2, '0');
				}
			}
		}, 1000);
}

