jQuery(document).ready(function ($) {

	var $show_more_loader	= $('.showmore-container #show-more-loader');
	var $show_more_button	= $('.showmore-container #show-more-orders');
	var $orders_table			= $('#recent-orders .recent-order-rows');
	var $search_table			= $('#found-orders .found-order-rows');
	var $search_button		= $('#order-search-trigger');
	var $search_loader		= $('#search-loader');
	var $clear_button			= $('#clear-search-btn');
	var $orders_header		= $('#orders-header');
	
	$show_more_button.on('click', function () {
		var last_order_id = $show_more_button.data('last-order');
		
		$show_more_button.hide();
		$show_more_loader.show();
		
		if ( last_order_id ) {
			$.ajax({
				type: "POST",
				url: '/wp-admin/admin-ajax.php',
				data: { "action": "more_recent_orders", "start_from": last_order_id },
				success: function (response) {
					if (response.success) {
						let data = response.data;
						$show_more_loader.hide();

						if (data.orders_left) { // there are still other orders to be shown
							$show_more_button.show();
						}

						$orders_table.append(data.found_orders_html);
						$show_more_button.data('last-order', data.last_order);
					}
				}
			});
		}
	});
	
	
	$search_button.on('click', function () {
				
		let search_string = $('#order-search').val();
		
		if ( search_string !== '' ) {
			if ( search_string.length < 2 ) {
				alert('Please enter at least two letters to search');
			}
			else {
				$search_button.hide();
				$show_more_button.hide();
				$clear_button.hide();
				$search_loader.css('display', 'inline-block');

				$.ajax({
					type: "POST",
					url: '/wp-admin/admin-ajax.php',
					data: { "action": "search_orders", "s": search_string },
					success: function (response) {
						if (response.success) {
							let data = response.data;
							$orders_header.html('Search results');
							$search_button.show();
							$search_loader.hide();
							$clear_button.show();
							$orders_table.hide();
							$search_table.show();
							
							if (data.found_orders_html) {
								$search_table.html(data.found_orders_html);
							}
							else {
								$search_table.html('<div class="order-name">Sorry, nothing found for your "' + search_string + '" request</div>');
							}
						}
					}
				});
			}
		}
	});
	
	document.getElementById('order-search').onkeydown = function(e){
   if(e.keyCode === 13){
     // submit
		 $search_button.click();
   }
	};
	
	
	$clear_button.on('click', function () {
		$orders_header.html('Order history');
		$search_button.show();
		$show_more_button.show();
		$clear_button.hide();
		$search_loader.hide();

		$orders_table.show();
		$search_table.hide();
	});
});