// @prepros-prepend ../node_modules/featherlight/src/featherlight.js

jQuery(document).ready(function ($) {
	
    var $btnRateProducts = $('.unrated-overlay');
    if ($btnRateProducts.length < 1) {
      return;
    }

    let currentProductId = null;
    let loading = false;

    // enable the submit rating button when user selected rating
    $(document).on('click', '.rsc__rating label', function () {
      $('.featherlight .btn-submit-rating').removeAttr('disabled');
			$('.rsc__rating label').removeClass('selected-rating');
			$(this).addClass('selected-rating');
    });

    // handle rating submission
    $(document).on('click', '.btn-submit-rating', function () {
      if ( loading ) {
        return;
      }
			currentProductId = $(this).data('product-id');
      loading = true;

      const $btn = $(this);
			const $error_msg = $(this).siblings('.rsc__error');
			const $loader = $(this).siblings('.rsc__loader');
			
      $btn.hide();
			$loader.show();
      $error_msg.text('');

      const rating = $('.rating-' + currentProductId + ' input[name="rating"]:checked').val();

			//console.log('THE CHOICE for' + currentProductId + ' is ' + rating);
			
      $.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {
          action: 'apd_rate_product',
          product_id: currentProductId,
          rating: rating
        },
        success: function (response) {
          if (!response.success) {
            $error_msg.text('Something is wrong. Please try again later. Error code: ' + response.data );
						loading = false;
						$loader.hide();
          } else {
            $('.featherlight .rating-system-content').addClass('thank-you');

            // increase rewards amount -- TODO
            const rewardsAmount = parseFloat($('.wallet-info-holder .value').text().replace(/[^0-9.]*/g, '')) || 0;
            $('.wallet-info-holder .value').text(`${(rewardsAmount + 5.00).toFixed(2)}`);
          }
        },
        error: function (error) {
        },
        complete: function () {
          loading = false;
          $btn.show();
					$loader.hide();
        }
      });
    });
  });