<?php
/**
 * Order Item Details
 * 
 * incoming params:
 * 
 * $order - WooCommerce order object
 * $item - 
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}

$is_single_product = get_post_meta( $item['product_id'], '_product_type_single', true ) == 'yes';
$is_deal_product = get_post_meta( $item['product_id'], '_product_big_deal', true ) == 'yes';
$order_is_completed = $order->get_status() == 'completed';

$item_meta   = $item->get_meta_data();

$coupon_code = '';

foreach ( $item_meta as $meta_item ) {
	if ( ( $meta_item->key == 'Coupon Code(s)' ) or ( $meta_item->key == 'License Code(s)' ) ) {
		$codes = $meta_item->value;

		if ( is_array( $codes ) ) {
			$coupon_code = implode( ', ', $codes );
		} else {
			$coupon_code = $codes;
		}
	}
}

if ( ! function_exists('prepare_apd_item_template_content') ) { 
	function prepare_apd_item_template_content( $mail_text, $billing_first_name, $coupon_code, $item ) {
		$html = "<div>[email_main_text]</div>";

		$html = str_replace( '[email_main_text]', $mail_text, $html );
		$html = str_replace( '{customer_name}', $billing_first_name, $html );
		$html = str_replace( '{coupon_code}', strip_tags( $coupon_code ), $html );
		$html = str_replace( '{gift_code}', $item['gift_code'], $html );
		$html = str_replace( '{developer_name}', $item['developer_name'], $html );
		$html = str_replace( '{developer_email}', $item['developer_email'], $html );
		$html = str_replace( '{company_name}', get_post_meta( $item['product_id'], 'company_name', true ), $html );
		$html = str_replace( '{redeem_link}', get_post_meta( $item['product_id'], 'url', true ), $html );
		$html = str_replace( '{url}', get_post_meta( $item['product_id'], '_redeem_link', true ), $html );

		return $html;
	}
}
?>

    <div class="product-name">
		<?php

		echo $item['name'];
		echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );

		?>
    </div>
    <div class="product-total">
		Price: <?php echo $order->get_formatted_line_subtotal( $item ); ?>
    </div>

<?php 
		if ( ! $is_single_product && ! $is_deal_product ) {
			
			$email_template_content = show_woocommerce_order_success_email_template( $order->get_id() );
			$woo                    = get_option( 'woocommerce_wc_apd_order_completed_settings' );
			$mail_text              = $woo['main_text'];
			
			if ( $email_template_content ) {
				echo '<div class="order_success_email_template">' . $email_template_content . '</div>';
			} 
			elseif ( $order_is_completed ) {
				
				$html = prepare_apd_item_template_content(
					$mail_text, 
					$order->billing_first_name, 
					$coupon_code, 
					$item
				);
				
				echo '<div class="order_success_email_template">' . $html . '</div>';
			}

		} 
		elseif ( $is_deal_product ) {
			
			$email_template_content = get_post_meta( $order->get_id(), 'email_template_content_bigdeal', true );
			$woo                    = get_option( 'woocommerce_wc_apd_bigdeal_order_completed_settings' );
			$mail_text              = $woo['main_text'];
						
			if ( $email_template_content ) {
					echo '<div class="order_success_email_template">' . $email_template_content . '</div>';
			} 
			elseif ( $order_is_completed ) {
				
				$html = prepare_apd_item_template_content(
					$mail_text, 
					$order->billing_first_name, 
					$coupon_code, 
					$item
				);

				echo '<div class="order_success_email_template">' . $html . '</div>';
			}
    } else {
			
			$email_template_content = apd_shop_order_success_email_template( $order->get_id(), $item['product_id'] );
			
			if ( $email_template_content ) {
				echo '<div class="order_success_email_template">' . $email_template_content . '</div>';
			} else {

				$email_heading  = get_post_meta( $item['product_id'], 'apd_product_email_heading', true );
				$email_template = get_post_meta( $item['product_id'], 'apd_product_email_template', true );
				
				$single_email = prepare_apd_item_template_content(
					$email_template, 
					$order->billing_first_name, 
					$coupon_code, 
					$item
				);
				
				if( $order_is_completed ) {
					echo '<div class="order_success_email_template">' . $email_heading . '</div>';
					echo '<div class="order_success_email_template">' . $single_email . '</div>';
				}
			}
		}

		$red_link   = get_field( 'url', $item['product_id'] );

		if ( $red_link ) {
			?>
            <strong><br/>The message above has also been forwarded to you by email.</strong>
			<?php
		}
		$order->display_item_downloads( $item );		
		?>
    
						
<?php if ( $show_purchase_note && $purchase_note ) : ?>
    <div class="product-purchase-note">
        <?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?>
    </div>
<?php endif; ?>