module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        dev_url: 'apd.local', // URL of your local install
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            gruntfile: [
                "Gruntfile.js"
            ],
            all: [
                'library/js/*.js'
            ]
        },
				sass: {                              // Task
						dist: {                            // Target
								options: {                       // Target options
										style: 'expanded'
								},
								files: {                         // Dictionary of files
										//'main.css': 'main.scss',       // 'destination': 'source'
										'./css-parts/rating.css': './css-parts/rating.scss',
										'./css-parts/shortcodes/account-recent-products.css': './css-parts/shortcodes/account-recent-products.scss',
										'./css-parts/shortcodes/account-recent-orders.css': './css-parts/shortcodes/account-recent-orders.scss',
										'./css-parts/shortcodes/account-edit-details.css': './css-parts/shortcodes/account-edit-details.scss'
								}
						}
				},
        concat: {
            options: {
                separator: "\n\n\n/*========================*/\n\n",
            },
            all_css: {
                src: [
                    'style.css',
                    'css-parts/*.css',
                    'css-parts/shortcodes/*.css'
                ],

                dest: 'style.compiled.css'
            },
						all_js: {
                src: [
                    'scripts.js',
                    'js-parts/*.js'
                ],

                dest: 'scripts.compiled.js'
            }
        },
        watch: {
            css: {
              files: ['css-parts/*.scss', 'css-parts/**/*.scss', 'css-parts/*.css', 'css-parts/**/*.css', 'js-parts/*.js'],
              tasks: ['build']
            }
        }
    });

    // Load tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
		grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Register tasks
    grunt.registerTask('default', [
        'sass', 'concat'
    ]);

    grunt.registerTask('build', [
        'sass', 'concat'
    ]);

};
