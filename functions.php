<?php

define('THEME_VERSION', '0.1');

require_once( __DIR__ . '/php/big-deal-shortcode.php');
require_once( __DIR__ . '/php/account-shortcodes.php');
require_once( __DIR__ . '/php/font-awesome.php');
require_once( __DIR__ . '/php/twenty-twelve.php');

// Add CSS styles and Google Fonts
function enqueue_theme_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array(), THEME_VERSION );
		
		// style.compiled.css consists of original styles.js and all CSS files that are located in /css-parts directory.
		// this file is made automatically by Grunt.
    wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/style.compiled.css', array(), THEME_VERSION);
	
		// Roboto & Open Sans, 400 and 700
		wp_enqueue_style( 'apd-fonts', 'https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,700;1,400;1,700&family=Roboto:ital,wght@0,400;0,700;1,400;1,700&display=swap', array(), null);
		
		// Featherlight
		wp_enqueue_style( 'featherlight', 'https://cdn.jsdelivr.net/npm/featherlight@1.7.14/release/featherlight.min.css', array(), null );
}

function enqueue_theme_scripts() {
	
		// scripts.compiled.js consists of original scripts.js and all JS files that are located in /js-parts directory.
		// this file is made automatically by Grunt.
    wp_enqueue_script('apd-scripts', get_stylesheet_directory_uri() . '/scripts.compiled.js', array('jquery'), THEME_VERSION);
}


add_action('wp_enqueue_scripts', 'enqueue_theme_styles');
add_action('wp_enqueue_scripts', 'enqueue_theme_scripts');



/* NEW THEME CODE START */


/* Youtube Popup short code */
function addsocialmediaembed () {

	global $post;
	$postId=$post->ID;

	$video_url_for_mainpage_banner = get_post_meta($postId,  'video_url_for_mainpage_banner',  true);

	$_regular_price = get_post_meta( get_the_ID(), '_regular_price', true);
	$_sale_price = get_post_meta( get_the_ID(), '_sale_price', true);
	?>

    <label class="buttonyoutube" for="youtube<?php echo $postId; ?>"><i aria-hidden="true" class="fab fa-youtube"></i></label>

		<input class="modal-state" id="youtube<?php echo $postId; ?>" type="checkbox" />
		<div class="modal">
			<label class="modal__bg" for="youtube<?php echo $postId; ?>"></label>
			<div class="modal__inner">
				<label class="modal__close" for="youtube<?php echo $postId; ?>"></label>
				<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="100%" height="443" type="text/html" 
				src="<?php echo $video_url_for_mainpage_banner; ?>">
				</iframe>
			</div>
		</div>
<?php
	
}

add_shortcode("youtubembedpopup", "addsocialmediaembed");


/* Souncloud Popup Short Code */
function addsoundcloudmediaembed () {

	global $post;
	$postId=$post->ID;

	$soundcloud_url = get_post_meta($postId,  'soundcloud_url',  true);

	?>

    <label class="buttonsoundcloud" for="scloud<?php echo $postId; ?>"><i aria-hidden="true" class="fas fa-volume-up"></i></label>

		<input class="modal-state" id="scloud<?php echo $postId; ?>" type="checkbox" />
		<div class="modal">
			<label class="modal__bg" for="scloud<?php echo $postId; ?>"></label>
			<div class="modal__inner">
				<label class="modal__close" for="scloud<?php echo $postId; ?>"></label>

					<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="100%" height="443" type="text/html" 
		src="<?php echo $soundcloud_url; ?>">
		</iframe>

			</div>
		</div>
<?php
	
}

add_shortcode("soundcloudembedpopup", "addsoundcloudmediaembed");

/* Get Page Title */
function page_title_sc( ){
   return get_the_title();
}
add_shortcode( 'page_title', 'page_title_sc' );

/* Short Description */
function page_excerpt_sc( ){
   return get_the_excerpt();
}
add_shortcode( 'page_excerpt', 'page_excerpt_sc' );

/* Permalink */
function page_permalink_sc( ){
   return get_permalink();
}
add_shortcode( 'page_permalink', 'page_permalink_sc' );


/* Regular Price */
function page_permalink_sc_price( ){
   return get_regular_price();
}
add_shortcode( 'product_price', 'page_permalink_sc_price' );






/* Youtube Popup short code */
function quickviewproduct () {
  
  global $post;
	$postId=$post->ID;

	$_regular_price = get_post_meta( get_the_ID(), '_regular_price', true);
	$_sale_price = get_post_meta( get_the_ID(), '_sale_price', true);

	?>
  
  
     <label class="buttonyoutube" for="quickview<?php echo $postId; ?>"><i aria-hidden="true" class="far fa-eye"></i></label>

<input class="modal-state" id="quickview<?php echo $postId; ?>" type="checkbox" />
<div class="modal">
  <label class="modal__bg" for="quickview<?php echo $postId; ?>"></label>
  <div class="modal__inner">
    <label class="modal__close" for="quickview<?php echo $postId; ?>"></label>

<?php echo do_shortcode('[page_title]') ?>
	  <?php echo do_shortcode('[page_excerpt]') ?>
	  <?php echo do_shortcode('[page_permalink]') ?>
	  <?php echo $_regular_price; ?>
	  <?php echo $_sale_price; ?>
    
  </div>
</div>

 
<?php
	
}

add_shortcode("quickviewproduct", "quickviewproduct");


/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options –> Reading
  // Return the number of products you wanna show per page.
  $cols = 12;
  return $cols;
}





/* Souncloud Popup Short Code */
function soundcloudaddonproductpage () {

	global $post;
	$postId=$post->ID;
	$soundcloud_url = get_post_meta($postId,  'soundcloud_url',  true);

	?>
		<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="100%" height="443" type="text/html" 
		src="<?php echo $soundcloud_url; ?>">
		</iframe>
	<?php
}

add_shortcode("soundcloudforproductpage", "soundcloudaddonproductpage");



/* Youtube Popup short code */
function youtubeforproductpagee () {

	global $post;
	$postId=$post->ID;

	$video_url_for_mainpage_banner = get_post_meta($postId,  'video_url_for_mainpage_banner',  true);

	$_regular_price = get_post_meta( get_the_ID(), '_regular_price', true);
	$_sale_price = get_post_meta( get_the_ID(), '_sale_price', true);

	?>
		<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="100%" height="443" type="text/html" 
		src="<?php echo $video_url_for_mainpage_banner; ?>">
		</iframe>
	<?php
	
}

add_shortcode("youtubeforproductpage", "youtubeforproductpagee");

/* jCountdown Date Shortcode */
function jcountdowndate () {

	global $post;
	$postId=$post->ID;

	$bigdeal_countdown_date = get_post_meta($postId,  'bigdeal_countdown_date',  true);
	$bigdeal_countdown_date = date("Y/m/d H:i:s", strtotime($bigdeal_countdown_date));

	echo $bigdeal_countdown_date; 
	
}

add_shortcode("jcountdowndateshortcode", "jcountdowndate");

/* Item added to cart remove */
add_filter( 'wc_add_to_cart_message_html', '__return_null' );

/* Product quantity remove single product page */
function custom_remove_all_quantity_fields( $return, $product ) {return true;}
add_filter( 'woocommerce_is_sold_individually','custom_remove_all_quantity_fields', 10, 2 );

// To change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
function woocommerce_custom_single_add_to_cart_text() {
    return __( 'Buy Now', 'woocommerce' ); 
}


/* NEW THEME CODE END */


/** OLD CODE - START ***/


// Set up the content width value based on the theme's design and stylesheet.

include_once(__DIR__ . '/inc/fix_get_total_spent.php');

if (!isset($content_width)) {
    $content_width = 625;
}

/**
 * check if rewards is being disabled and the site is offering base price
 *
 * @param int $product_id
 *
 * @return bool
 */
function is_disable_rewards_and_offer_base_price($product_id = 0) {
    $disable_rewards_and_offer_base_price = carbon_get_theme_option('_disable_rewards_and_offer_base_price');

    if ($disable_rewards_and_offer_base_price === 'yes') {
        return true;
    }

    if ($product_id) {
        $disable_rewards_and_offer_base_price_for_custom_products = carbon_get_theme_option('_disable_rewards_and_offer_base_price_for_custom_products');
        $disable_rewards_and_offer_base_price_for_custom_products = is_array($disable_rewards_and_offer_base_price_for_custom_products) ? wp_list_pluck($disable_rewards_and_offer_base_price_for_custom_products, 'id') : array();

        return in_array($product_id, $disable_rewards_and_offer_base_price_for_custom_products);
    }

    return false;
}

/**
 * return product price
 * @param \WC_Product $product
 * @return double
 */
function apd_get_product_price($product) {
    if (is_disable_rewards_and_offer_base_price($product->get_id())) {
        return get_post_meta($product->get_id(), '_minumum_price', true);
    }

    $sale = $product->get_sale_price();
    if ($sale) {
        return $sale;
    }

    return $product->get_regular_price();
}

add_filter('wp_nav_menu_items', 'sk_wcmenucart', 10, 2);
function sk_wcmenucart($menu, $args) {
    // Check if WooCommerce is active and add a new item to a menu assigned to Primary Navigation Menu location
    if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) || 'Account Menu' !== $args->menu) {
        return $menu;
    }
    ob_start();
    global $woocommerce;
    $viewing_cart = __('View your shopping cart', 'your-theme-slug');
    $start_shopping = __('Start shopping', 'your-theme-slug');
    $current_user = wp_get_current_user();
    $menu_item = '';

    $cart_url = wc_get_cart_url();
    $shop_page_url = get_permalink(wc_get_page_id('shop'));
    $cart_contents_count = $woocommerce->cart->cart_contents_count;
    $cart_contents = sprintf(_n('%d item', '%d items', $cart_contents_count, 'your-theme-slug'), $cart_contents_count);
    $cart_total = $woocommerce->cart->get_cart_total();
    // Uncomment the line below to hide nav menu cart item when there are no items in the cart
    // if ( $cart_contents_count > 0 ) {
    if ($cart_contents_count == 0) {
        $menu_item .= '<li class="right" data-icon="cart"><a  class="wcmenucart-contents" href="' . $cart_url . '" title="' . $start_shopping . '">';
    } else {
        $menu_item .= '<li class="right" data-icon="cart"><a  class="wcmenucart-contents" href="' . $cart_url . '" title="' . $viewing_cart . '">';
    }

    $menu_item .= '';

    $menu_item .= 'Cart (' . $cart_contents_count . ')';
    $menu_item .= '</a></li>';
    // Uncomment the line below to hide nav menu cart item when there are no items in the cart
    // }
    if ($current_user->ID) {
        //$reward_points = (get_user_meta($current_user->ID, '_reward_points', true)) ? get_user_meta($current_user->ID, '_reward_points', true) : 0;
        $menu_item .= '<li class="right" data-icon="wishlist"><a  href="' . get_permalink(get_page_by_path("wishlist")) . '">Wishlist</a></li>';
        //$menu_item .= '<li class="right" data-icon="wallet"><a  href="' . get_permalink(get_page_by_path('wallet')) . '" id="' . uniqid() . '" class="your_rewards">Your rewards: $' . number_format($reward_points, 2, '.', '') . '</a></li>';
    }
    echo $menu_item;
    $social = ob_get_clean();

    return $menu . $social;
}

//add_action('woocommerce_created_customer', 'admin_email_on_registration');
function admin_email_on_registration() {
    $user_id = get_current_user_id();
    wp_new_user_notification($user_id);
}

//add_filter( 'woocommerce_payment_complete_order_status', 'virtual_order_payment_complete_order_status', 10, 2 );

function virtual_order_payment_complete_order_status($order_status, $order_id) {
    $order = new WC_Order($order_id);

    if ('processing' == $order_status &&
        ('on-hold' == $order->status || 'pending' == $order->status || 'failed' == $order->status)
    ) {
        $virtual_order = null;

        if (count($order->get_items()) > 0) {
            foreach ($order->get_items() as $item) {
                if ('line_item' == $item['type']) {
                    $_product = $order->get_product_from_item($item);

                    if (!$_product->is_virtual()) {
                        // once we've found one non-virtual product we know we're done, break out of the loop
                        $virtual_order = false;
                        break;
                    } else {
                        $virtual_order = true;
                    }
                }
            }
        }

        // virtual order, mark as completed
        if ($virtual_order) {
            return 'completed';
        }
    }

    // non-virtual order, return original status
    return $order_status;
}

// Add Shortcode
function LTO_Company_Name($item_id, $item, $order) {
    do_action('woocommerce_order_item_meta_start', $item_id, $item, $order);
    $order->display_item_meta($item);
    $company_name = get_field('lto_company_name', $item['product_id']);

    return $company_name;
}

add_shortcode('LTO_Company_Name', 'LTO_Company_Name');

function company_url($item_id, $item, $order) {
    do_action('woocommerce_order_item_meta_start', $item_id, $item, $order);
    $order->display_item_meta($item);
    $company_url = get_field('url', $item['product_id']);

    return $company_url;
}

add_shortcode('LTO_Company_URL', 'company_url');
// Empty cart message
add_filter('wc_empty_cart_message', 'custom_wc_empty_cart_message');

function custom_wc_empty_cart_message() {
    return 'Your cart is currently empty!';
}

// Tracking Code to get order value for Adroll
add_action('woocommerce_thankyou', 'my_custom_tracking');

function my_custom_tracking($order_id) {
    // Lets grab the order
    $order = wc_get_order($order_id);

    /**
     * Put your tracking code here
     * You can get the order total etc e.g. $order->get_total();
     */

    // This is the order total
    $order->get_total();

    // This is how to grab line items from the order
    $line_items = $order->get_items();

    // This loops over line items
    foreach ($line_items as $item) {
        // This will be a product
        $product = $order->get_product_from_item($item);

        // This is the products SKU
        $sku = $product->get_sku();

        // This is the qty purchased
        $qty = $item['qty'];

        // Line item total cost including taxes and rounded
        $total = $order->get_line_total($item, true, true);

        // Line item subtotal (before discounts)
        $subtotal = $order->get_line_subtotal($item, true, true);
    }
}

function get_product_rewards_percentage($product_id) {
    // custom rewards earning event active
    $is_global_rewards_active = carbon_get_theme_option('_active_global_reward_percentage');
    if ($is_global_rewards_active == 'yes') {
        $rewards_percentage = carbon_get_theme_option('_global_reward_percentage');
        return intval($rewards_percentage);
    }

    // no event
    $rewards_percentage = get_post_meta($product_id, '_reward_points', true);
    return intval($rewards_percentage);
}

function apd_add_product_type_field() {
    global $woocommerce, $post;
    echo '<div class="options_group">';

    woocommerce_wp_checkbox(
        array(
            'id' => '_product_type_single',
            'label' => __('Shop Product', 'woocommerce'),
            'desc_tip' => 'true',
            'description' => __('Check this field if the product should appear in the Shop section', 'woocommerce')
        ));

    woocommerce_wp_checkbox(
        array(
            'id' => '_product_big_deal',
            'label' => __('Big Deal Product', 'woocommerce'),
            'desc_tip' => 'true',
            'description' => __('Check this field for the big deal product', 'woocommerce')
        ));

    woocommerce_wp_text_input(
        array(
            'id' => '_reward_points',
            'label' => __('Reward points value %', 'woocommerce'),
            'placeholder' => '',
            'description' => __('Enter the reward points value in percents', 'woocommerce'),
            'type' => 'number',
            'value' => (get_post_meta($post->ID, '_reward_points', true)) ? get_post_meta($post->ID, '_reward_points', true) : 10,
            'custom_attributes' => array(
                'step' => 'any',
                'min' => '0'
            )
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => '_minumum_price',
            'label' => __('Minimum price', 'woocommerce'),
            'placeholder' => '',
            'description' => __('Enter the minumum price', 'woocommerce'),
            'type' => 'number',
            'value' => (get_post_meta($post->ID, '_minumum_price', true)) ? get_post_meta($post->ID, '_minumum_price', true) : 0,
            'custom_attributes' => array(
                'step' => 'any',
                'min' => '0'
            )
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => '_redeem_link',
            'label' => __('Redeem link', 'woocommerce'),
            'placeholder' => '',
            'description' => __('Enter the redeem link', 'woocommerce'),
            'type' => 'text',
            'value' => (get_post_meta($post->ID, '_redeem_link', true)) ? get_post_meta($post->ID, '_redeem_link', true) : '',
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => '_blue_light_sort',
            'label' => __('Blue Light Special sort', 'woocommerce'),
            'placeholder' => '',
            'description' => __('Enter the ordering value. The lower the number the higher the position', 'woocommerce'),
            'type' => 'number',
            'value' => (get_post_meta($post->ID, '_blue_light_sort', true)) ? get_post_meta($post->ID, '_blue_light_sort', true) : 0,
            'custom_attributes' => array(
                'step' => 'any',
            )
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => '_trending_sort',
            'label' => __('Trending sort', 'woocommerce'),
            'placeholder' => '',
            'description' => __('Enter the ordering value. The lower the number the higher the position', 'woocommerce'),
            'type' => 'number',
            'value' => (get_post_meta($post->ID, '_trending_sort', true)) ? get_post_meta($post->ID, '_trending_sort', true) : 0,
            'custom_attributes' => array(
                'step' => 'any',
            )
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => '_new_arrivals_sort',
            'label' => __('New arrivals sort', 'woocommerce'),
            'placeholder' => '',
            'description' => __('Enter the ordering value. The lower the number the higher the position', 'woocommerce'),
            'type' => 'number',
            'value' => (get_post_meta($post->ID, '_new_arrivals_sort', true)) ? get_post_meta($post->ID, '_new_arrivals_sort', true) : 0,
            'custom_attributes' => array(
                'step' => 'any',
            )
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => '_gift_certificates_sort',
            'label' => __('Gift Certificates sort', 'woocommerce'),
            'placeholder' => '',
            'description' => __('Enter the ordering value. The lower the number the higher the position', 'woocommerce'),
            'type' => 'number',
            'value' => (get_post_meta($post->ID, '_gift_certificates_sort', true)) ? get_post_meta($post->ID, '_gift_certificates_sort', true) : 0,
            'custom_attributes' => array(
                'step' => 'any',
            )
        )
    );

    echo '</div>';
}

add_action('woocommerce_product_options_general_product_data', 'apd_add_product_type_field');

function apd_save_product_type_value($post_id) {
    $product_type = isset($_POST['_product_type_single']) ? 'yes' : 'no';
    update_post_meta($post_id, '_product_type_single', $product_type);

    $product_type = isset($_POST['_product_big_deal']) ? 'yes' : 'no';
    update_post_meta($post_id, '_product_big_deal', $product_type);

    $reward_points = $_POST['_reward_points'];

    if (!empty($reward_points)) {
        update_post_meta($post_id, '_reward_points', esc_attr($reward_points));
    }

    $min_price = $_POST['_minumum_price'];

    if (!empty($min_price)) {
        update_post_meta($post_id, '_minumum_price', esc_attr($min_price));
    }

    $redeem_link = $_POST['_redeem_link'];

    if (!empty($redeem_link)) {
        update_post_meta($post_id, '_redeem_link', esc_attr($redeem_link));
    }

    $blue_light = $_POST['_blue_light_sort'];

    update_post_meta($post_id, '_blue_light_sort', esc_attr($blue_light));

    $trending = $_POST['_trending_sort'];

    update_post_meta($post_id, '_trending_sort', esc_attr($trending));

    $new_arrivals = $_POST['_new_arrivals_sort'];

    update_post_meta($post_id, '_new_arrivals_sort', esc_attr($new_arrivals));

    $gift_certificates = $_POST['_gift_certificates_sort'];

    update_post_meta($post_id, '_gift_certificates_sort', esc_attr($gift_certificates));
}

add_action('woocommerce_process_product_meta', 'apd_save_product_type_value');

function apd_user_rewards_diplay_column($rewards) {
    $rewards['reward_points'] = 'Reward Points';

    return $rewards;
}

add_filter('user_rewards', 'apd_user_rewards_diplay_column', 10, 1);

function apd_rewards_field_user_table($column) {
    $column['reward_points'] = 'Reward Points';

    return $column;
}

add_filter('manage_users_columns', 'apd_rewards_field_user_table');

function apd_rewards_field_retrieve_value($val, $column_name, $user_id) {
    switch ($column_name) {
        case 'reward_points' :
            $reward_points = get_the_author_meta('_reward_points', $user_id);

            return ($reward_points) ? $reward_points : '0';
            break;
        default:
    }

    return $val;
}

add_filter('manage_users_custom_column', 'apd_rewards_field_retrieve_value', 10, 3);

function apd_fill_rewards_button($wp_admin_bar) {
    $args = array(
        'id' => 'fill-rewards-button',
        'title' => 'Fill Rewards',
        'href' => '/wp-admin/users.php?fill_rewards=all',

    );
    $wp_admin_bar->add_node($args);
}

add_action('admin_bar_menu', 'apd_fill_rewards_button', 10000);

function apd_fill_rewards_action() {
    if (isset($_GET['fill_rewards']) && $_GET['fill_rewards'] == 'all' && get_option('_filled_rewards') != 1) {
        $customers = get_users();
        if (count($customers) > 0) {
            $total = 0;
            foreach ($customers as $customer) {
                $total = wc_get_customer_total_spent($customer->ID);
                update_user_meta($customer->ID, '_reward_points', number_format(($total / 10), 2, '.', ''));
                $total = 0;
            }
        }
        update_site_option('_filled_rewards', 1);
    } else {
        return;
    }
}

add_action('admin_init', 'apd_fill_rewards_action');

function apd_reward_points_edit_user_profile($user) { ?>

    <h3>Edit customer's reward points</h3>

    <table class="form-table">

        <tr>
            <th><label for="twitter">Reward Points</label></th>

            <td>
                <input type="text" name="reward_points" id="reward_points"
                       value="<?php echo esc_attr(get_the_author_meta('_reward_points', $user->ID)); ?>"
                       class="regular-text" placeholder="10"/><br/>
                <span class="description">Please enter customer's reward points</span>
            </td>
        </tr>

    </table>
<?php }

add_action('show_user_profile', 'apd_reward_points_edit_user_profile');
add_action('edit_user_profile', 'apd_reward_points_edit_user_profile');

function apd_reward_points_save($user_id) {
    if (!current_user_can('edit_user', $user_id)) {
        return false;
    }
    update_user_meta($user_id, '_reward_points', $_POST['reward_points']);
}

add_action('personal_options_update', 'apd_reward_points_save');
add_action('edit_user_profile_update', 'apd_reward_points_save');

add_filter('bulk_actions-users', 'apd_register_reward_points_bulk_actions');

function apd_register_reward_points_bulk_actions($bulk_actions) {
    $bulk_actions['update_rewards'] = __('Update reward points', 'update_rewards');

    return $bulk_actions;
}

add_action('admin_footer', 'apd_mass_update_rewards_field');

function apd_mass_update_rewards_field() {
    $screen = get_current_screen();
    if ($screen->id != "users")   // Only add to users.php page
    {
        return;
    }
    ?>
    <script type="text/javascript">
      jQuery(document).ready(function ($) {
        $('.tablenav.top .clear, .tablenav.bottom .clear').before('<div class="alignleft actions"><label for="mass_reward_point">Mass update reward point: </label><input type="text" id="mass_reward_point" name="mass_reward_point" size="1" value=""/></div>');
        var form = $('#mass_reward_point').closest("form");
        $(form).on('submit', function (e) {
          var val = $('#mass_reward_point').val();
          $(form).append('<input type="hidden" name="mass_reward_points" value="' + val + '"/>');
        });
      });

    </script>
    <?php
}

function apd_mass_update_rewards($redirect_to, $action_name, $post_ids) {
    if ($action_name == 'update_rewards' && $_REQUEST['mass_reward_points'] > 0) {
        foreach ($_REQUEST['users'] as $user) {
            update_user_meta($user, '_reward_points', $_REQUEST['mass_reward_points']);
        }
    }

    return $redirect_to;
}

add_filter('handle_bulk_actions-users', 'apd_mass_update_rewards', 10, 3);

function apd_product_custom_price($cart_item_data) {
    $current_user = wp_get_current_user();
    $cart_item_data['use_rewards'] = 0;
    $unique_cart_item_key = md5(microtime() . rand());
    $cart_item_data['unique_key'] = $unique_cart_item_key;
    if (isset($_POST['use_rewards']) && $_POST['use_rewards'] >= 0 && !empty($_POST['use_rewards'])) {
        if ((float)get_user_meta($current_user->ID, '_reward_points', true) >= $_POST['use_rewards']) {
            $cart_item_data['use_rewards'] += (float)$_POST['use_rewards'];
            $bonus_points = (float)get_user_meta($current_user->ID, 'temp_reward_points', true);
            if ($bonus_points > 0 && $_POST['use_rewards'] <= $bonus_points) {
                $bonus_points = $bonus_points - $_POST['use_rewards'];
            } elseif ($bonus_points > 0 && $_POST['use_rewards'] > $bonus_points) {
                $bonus_points = 0;
            }
            $current_points = (float)get_user_meta($current_user->ID, '_reward_points', true);
            $current_points = number_format($current_points - $_POST['use_rewards'], 2, '.', '');
            update_user_meta($current_user->ID, '_reward_points', $current_points);
            #update_user_meta( $current_user->ID, 'temp_reward_points', $bonus_points );
        }
    }

    return $cart_item_data;
}

add_filter('woocommerce_add_cart_item_data', 'apd_product_custom_price', 10);

/**
 * @param \WC_Cart $cart_object
 */
function apd_apply_custom_price_to_cart_item($cart_object) {
    // disable rewards and offer base price
    $disable_rewards_and_offer_base_price = is_disable_rewards_and_offer_base_price();
    if ($disable_rewards_and_offer_base_price) {
        foreach ($cart_object->cart_contents as $key => $cart_item) {
            /**
             * @var \WC_Product $item_product
             */
            $item_product = $cart_item['data'];

            $item_product->set_price(apd_get_product_price($item_product));
        }
        return;
    }

    // rewards
    if (!WC()->session->__isset('reload_checkout')) {
        foreach ($cart_object->cart_contents as $key => $value) {
            if (isset($value['smart_offers'])) continue;

            // rewards
            if ((isset($value['use_rewards']) && $value['use_rewards'] >= 0) || isset($_POST['cart'][$key]['rewards'])) {
                $cart_rewards = (!isset($_POST['cart'])) ? 0 : $_POST['cart'][$key]['rewards'];
                $cart = (isset($_POST['cart'])) ? $_POST['cart'] : array();
                if (array_key_exists($key, $cart) && $value['use_rewards'] != $cart_rewards) {
                    $current_user = wp_get_current_user();
                    $virtual_points = (float)get_user_meta($current_user->ID, '_reward_points', true) + $value['use_rewards'] - $_POST['cart'][$key]['rewards'];
                    if ($virtual_points > 0) {
                        if ($virtual_points >= $_POST['cart'][$key]['rewards']) {
                            $bonus_points = (float)get_user_meta($current_user->ID, 'temp_reward_points', true);
                            if ($bonus_points > 0 && $_POST['cart'][$key]['rewards'] <= $bonus_points) {
                                $bonus_points = $bonus_points - $_POST['cart'][$key]['rewards'];
                            } elseif ($bonus_points > 0 && $_POST['cart'][$key]['rewards'] > $bonus_points) {
                                $bonus_points = 0;
                            }
                            $current_points = number_format($virtual_points, 2, '.', '');
                            update_user_meta($current_user->ID, '_reward_points', $current_points);
                            #update_user_meta( $current_user->ID, 'temp_reward_points', $bonus_points );
                        } else {
                            update_user_meta($current_user->ID, '_reward_points', $virtual_points + $_POST['cart'][$key]['rewards']);
                        }
                        $cart_object->cart_contents[$key]['use_rewards'] = $_POST['cart'][$key]['rewards'];
                        $value['use_rewards'] = $_POST['cart'][$key]['rewards'];
                    }
                }
                WC()->session->set(
                    $value['product_id'], array(
                        'use_rewards' => $value['use_rewards']
                    )
                );

                $_pf = new WC_Product_Factory();
                $product = $_pf->get_product($value['product_id']);
                if (!$product->is_type('variable')) {
                    if ($value['quantity'] > 1) {
                        $price = $product->get_price() - (float)$value['use_rewards'] / $value['quantity'];
                    } else {
                        $price = $product->get_price() - (float)$value['use_rewards'];
                    }

                    $price = round($price, 2);

                    if ($price >= (float)get_post_meta($value['product_id'], '_minumum_price', true)) {
                        $value['data']->set_price($price);
                    } else {
                        $cart_object->cart_contents[$key]['use_rewards'] = 0;
                    }
                }
            }
        }
    }
}

add_action('woocommerce_before_calculate_totals', 'apd_apply_custom_price_to_cart_item');

function get_cart_items_from_session($item, $values, $key) {
    if (array_key_exists('use_rewards', $values)) {
        $item['use_rewards'] = $values['use_rewards'];
    }

    return $item;
}

add_filter('woocommerce_get_cart_item_from_session', 'get_cart_items_from_session', 1, 3);

function apd_store_rewards_to_order_meta($item_id, $item_values, $item_key) {
    $developer_id = '';
    $developer_name = '';
    $developer_email = '';
    $session_data = WC()->session->get($item_values['product_id']);
    if (!empty($session_data['use_rewards'])) {
        wc_update_order_item_meta($item_id, '_used_rewards', sanitize_text_field($session_data['use_rewards']));
    }
    if (get_post_meta($item_values['product_id'], '_product_type_single', true) == 'yes') {
        $objects = wp_get_post_terms($item_values['product_id'], 'developer');
        if (count($objects) > 0) {
            $developer_id = $objects[0]->term_id;
            $developer_name = $objects[0]->name;
            $developer_email = get_term_meta($developer_id, 'developer_email', true);
        }
        wc_update_order_item_meta($item_id, 'shop_product', 1);
        wc_update_order_item_meta($item_id, 'developer_name', $developer_name);
        wc_update_order_item_meta($item_id, 'developer_email', $developer_email);
    } elseif (get_post_meta($item_values['product_id'], '_product_big_deal', true) == 'yes') {
        wc_update_order_item_meta($item_id, 'bigdeal', 1);
    } else {
        wc_update_order_item_meta($item_id, 'deal_product', 1);
    }
}

add_action('woocommerce_new_order_item', 'apd_store_rewards_to_order_meta', 1, 3);

add_action('woocommerce_checkout_update_order_meta', function ($order_id, $posted) {
    $order = wc_get_order($order_id);
    $shop_product = false;
    $deal_product = false;
    $big_deal_product = false;
    $order->update_meta_data('order_type', 'mixed');
    $items = $order->get_items();
    foreach ($items as $key => $item) {
        if (get_post_meta($item['product_id'], '_product_type_single', true) == 'yes') {
            $shop_product = true;
        } elseif (get_post_meta($item['product_id'], '_product_big_deal', true) == 'yes') {
            $big_deal_product = true;
        } else {
            $deal_product = true;
        }
    }

    if ($shop_product && !$deal_product && !$big_deal_product) {
        $order->update_meta_data('order_type', 'shop');
    }

    if ($big_deal_product && !$shop_product && !$deal_product) {
        $order->update_meta_data('order_type', 'bigdeal');
    }

    if ($deal_product && !$shop_product && !$big_deal_product) {
        $order->update_meta_data('order_type', 'deal');
    }

    $order->save();
}, 10, 2);

function apd_order_completed($order_id) {
    global $woocommerce;

    $order = wc_get_order($order_id);
    $customer_id = $order->get_user_id();
    $order_items = $order->get_items();
    $current_points = get_user_meta($customer_id, '_reward_points', true);
    $developers = array();
    $earned = 0;
    foreach ($order_items as $id => $item) {
        $item_total = wc_get_order_item_meta($id, '_line_total', true);
        // $reward_percentage = get_post_meta($item['product_id'], '_reward_points', true);
        $reward_percentage = get_product_rewards_percentage($item['product_id']);
        $earned += $item_total * $reward_percentage / 100;
        $current_points = number_format($current_points + $item_total * $reward_percentage / 100, 2, '.', '');
        //wc_update_order_item_meta( $id, '_used_rewards', '0' );
        if (get_post_meta($item['product_id'], '_product_type_single', true) == 'yes') {
            $developers[] = array(
                'name' => $item['name'],
                'developer_name' => $item['developer_name'],
                'developer_email' => $item['developer_email'],
                'product_price' => $item_total,
                'rewards_used' => wc_get_order_item_meta($id, '_used_rewards', true)
            );
        }
    }
    update_user_meta($customer_id, '_reward_points', $current_points);
    $mailer = $woocommerce->mailer();
    add_filter('woocommerce_email_styles', 'apd_woocommerce_email_styles');
    $image = '<img class="header_logo" src="' . esc_url(get_template_directory_uri()) . '/images/shop/logo_shop.png"/>';
    if (!empty($developers)) {
        foreach ($developers as $developer) {
            $email_message = 'Hello!<br/>
            Your item has been sold! Here is the details below:
            Item name: ' . $developer['name'] . '<br/>
            Price: $' . $developer['product_price'] . '<br/>
            Rewards used: $' . $developer['rewards_used'] . '<br/><br/>
            Best Regards,<br/>
            APD Team
            ';
            $message = $mailer->wrap_message(
                sprintf(__('Audio Plugin Deals: Your Item Has Been Sold!'), $order->get_order_number()), $email_message);
            $message = str_replace('[header_image]', $image, $message);
            if (get_post_meta($order_id, 'order_type', true) == 'shop') {
                $message = str_replace('#d73e1f', '#2773c9', $message);
            }
            $message = str_replace('#e78b79', '#2773c9', $message);
            $message = str_replace('#d73e1f', '#2773c9', $message);
            $mailer->send($developer['developer_email'], sprintf(__('Audio Plugin Deals: Your Item Has Been Sold!'), $order->get_order_number()), $message);
        }
    }
    $reward_message = 'Dear ' . $order->get_billing_first_name() . '.<br/>
    <b>You\'ve got money!</b><br/><br/>
    From your last order, you earned $' . (float)(floor($earned * 100) / 100) . '!<br/></br>
    <center><img src="' . esc_url(get_template_directory_uri()) . '/images/shop/email_wallet.png"></center>
    <center>Your rewards wallet balance:</center>
    <center><span style="font-size: 24px;"><b><a style="text-decoration:none;color:#339966" href="https://audioplugin.deals/wallet">$' . $current_points . '</a></b></span></center> <br/>
    <span style="text-align: left">Don\'t forget that for every order you make in <a style="color:#d73e1d" href="https://audioplugin.deals/">The Deal</a> section or in <a href="https://audioplugin.deals/shop/">The Shop</a>, you get at least 10% of the amount you spend added to your wallet for future purchases.</span><br/><br/>
    <div style="margin-left: auto;margin-right: auto;float: none;max-width:150px;margin-bottom:20px;"><table border="0" cellpadding="0" cellspacing="0" style="background-color:#2773c9; border:1px solid #353535; border-radius:5px;"><tr><td align="center" valign="middle" style="color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; letter-spacing:-.5px; line-height:150%; padding-top:10px; padding-right:30px; padding-bottom:10px; padding-left:30px;"><a href="https://audioplugin.deals/shop/" target="_blank" style="color:#FFFFFF; text-decoration:none;">Shop Now</a></td></tr></table></div>
    ';
    $msg = $mailer->wrap_message(
        sprintf(__('Audio Plugin Deals - The Shop: Rewards Money Balance'), $order->get_order_number()), $reward_message);

    $msg = str_replace('[header_image]', $image, $msg);
    if (get_post_meta($order_id, 'order_type', true) == 'shop') {
        $msg = str_replace('#e78b79', '#2773c9', $msg);
        $msg = str_replace('#d73e1f', '#2773c9', $msg);
    }

    $mailer->send($order->get_billing_email(), __('Audio Plugin Deals - The Shop: Rewards Money Balance'), $msg);
    remove_filter('woocommerce_email_styles', 'apd_woocommerce_email_styles');
}

function apd_woocommerce_email_styles($css) {
    $css .= "#template_header { background-color: #2773c9; }";
    $css .= "a {color:#2773c9;}";
    $css .= "#credit p{color:#2773c9;} ";
    $css .= ".link{color:#2773c9;}";
    $css .= "h2 {color:#2773c9;}";

    return $css;
}

add_action('woocommerce_order_status_completed', 'apd_order_completed');

function apd_order_cancelled($order_id) {
    $order = wc_get_order($order_id);
    $customer_id = $order->get_user_id();
    $order_items = $order->get_items();
    $current_points = get_user_meta($customer_id, '_reward_points', true);
    $temp_reward_bonus = get_option('temp_rewards_points', true);
    $user_temp_points = get_user_meta($customer_id, 'temp_reward_points', true);
    foreach ($order_items as $id => $item) {
        $used_rewards = wc_get_order_item_meta($id, '_used_rewards', true);
        if ($used_rewards) {
            $current_points = $current_points + $used_rewards;
            if ($used_rewards <= $temp_reward_bonus) {
                $user_temp_points = $user_temp_points + $used_rewards;
            } else {
                $user_temp_points = $temp_reward_bonus;
            }
        } else {
            $current_points = $current_points;
        }
        //wc_update_order_item_meta( $id, '_used_rewards', '0' );
    }
    update_user_meta($customer_id, '_reward_points', number_format($current_points, 2, '.', ''));
    #update_user_meta( $customer_id, 'temp_reward_points', number_format( $user_temp_points, 2, '.', '' ) );
}

add_action('woocommerce_order_status_cancelled', 'apd_order_cancelled');

function apd_order_refunded($order_id) {
    $order = wc_get_order($order_id);
    $customer_id = $order->get_user_id();
    $order_items = $order->get_items();
    $current_points = get_user_meta($customer_id, '_reward_points', true);
    $temp_reward_bonus = get_option('temp_rewards_points', true);
    $user_temp_points = get_user_meta($customer_id, 'temp_reward_points', true);
    foreach ($order_items as $id => $item) {
        $used_rewards = wc_get_order_item_meta($id, '_used_rewards', true);
        if ($used_rewards) {
            $current_points = $current_points + $used_rewards;
            if ($used_rewards <= $temp_reward_bonus) {
                $user_temp_points = $user_temp_points + $used_rewards;
            } else {
                $user_temp_points = $temp_reward_bonus;
            }
        } else {
            $current_points = $current_points;
        }
        //wc_update_order_item_meta( $id, '_used_rewards', '0' );
    }
    update_user_meta($customer_id, '_reward_points', number_format($current_points, 2, '.', ''));
    #update_user_meta( $customer_id, 'temp_reward_points', number_format( $user_temp_points, 2, '.', '' ) );
}

add_action('woocommerce_order_status_refunded', 'apd_order_refunded');

function apd_remove_cart_item($cart_item_key, $cart) {
    if (isset($cart->cart_contents[$cart_item_key]['use_rewards'])) {
        $current_user = wp_get_current_user();
        $current_points = get_user_meta($current_user->ID, '_reward_points', true);
        $temp_reward_bonus = get_option('temp_rewards_points', true);
        $user_temp_points = get_user_meta($current_user->ID, 'temp_reward_points', true);
        $current_points = number_format($current_points + $cart->cart_contents[$cart_item_key]['use_rewards'], 2, '.', '');
        if ($cart->cart_contents[$cart_item_key]['use_rewards'] <= $temp_reward_bonus) {
            $user_temp_points = $user_temp_points + $cart->cart_contents[$cart_item_key]['use_rewards'];
        } else {
            $user_temp_points = $temp_reward_bonus;
        }
        update_user_meta($current_user->ID, '_reward_points', number_format($current_points, 2, '.', ''));
        #update_user_meta( $current_user->ID, 'temp_reward_points', number_format( $user_temp_points, 2, '.', '' ) );
    }
}

add_action('woocommerce_remove_cart_item', 'apd_remove_cart_item', 10, 2);

function apd_developer_register() {
    register_taxonomy('developer', array('product'), array(
        'labels' => array(
            'name' => 'Developers',
            'singular_name' => 'Developers',
            'search_items' => 'Search Developer',
            'popular_items' => 'Popular Developers',
            'all_items' => 'All Developers',
            'parent_item' => 'Parent Developer',
            'parent_item_colon' => 'Parent Developer:',
            'edit_item' => 'Edit Developer',
            'update_item' => 'Update Developer',
            'add_new_item' => 'Add New Developer',
            'new_item_name' => 'New Developer',
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'developer'),
    ));
    flush_rewrite_rules();
}

add_action('widgets_init', 'apd_developer_register');

function apd_add_developer_email_field() {
    ?>

    <div class="form-field term-slug-wrap">
        <label><?php _e('Developer email', 'woocommerce'); ?></label>
        <input name="developer_email" id="developer_email" type="text" value="" size="40"/>
        <p>Enter here an email of Developer for email reporting</p>
    </div>
    <div class="form-field term-slug-wrap">
        <label><?php _e('Payout method', 'woocommerce'); ?></label>
        Paypal: <input type="radio" name="developer_payout" value="Paypal">
        Check: <input type="radio" name="developer_payout" value="Check">
        <p>Select preferable payout method for the developer</p>
    </div>
    <?php
}

add_action('developer_add_form_fields', 'apd_add_developer_email_field');

function apd_save_developer_company_email_field($term_id) {
    if (isset($_POST['developer_email'])) {
        update_term_meta($term_id, 'developer_email', $_POST['developer_email']);
    }
    if (isset($_POST['developer_payout'])) {
        update_term_meta($term_id, 'developer_method', $_POST['developer_payout']);
    }
}

add_action('created_developer', 'apd_save_developer_company_email_field', 10, 1);
add_action('edited_developer', 'apd_save_developer_company_email_field', 10, 3);

function apd_edit_lto_developer_email_field($term, $taxonomy) {
    $developer_email = get_term_meta($term->term_id, 'developer_email', true);
    $developer_method = get_term_meta($term->term_id, 'developer_method', true);
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label><?php _e('Developer email', 'woocommerce'); ?></label></th>
        <td>
            <div>
                <input type="text" id="developer_email" name="developer_email" value="<?php echo $developer_email; ?>"/>
            </div>
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top"><label><?php _e('Payout method', 'woocommerce'); ?></label></th>
        <td>
            <div>
                Paypal: <input type="radio" name="developer_payout"
                               value="Paypal" <?php echo ($developer_method == 'Paypal') ? 'checked' : '' ?>>
                Check: <input type="radio" name="developer_payout"
                              value="Check" <?php echo ($developer_method == 'Check') ? 'checked' : '' ?>>
            </div>
        </td>
    </tr>
    <?php
}

add_action('developer_edit_form_fields', 'apd_edit_lto_developer_email_field', 10, 2);

function apd_customer_apd_report() {
    global $woocommerce;
    $mailer = $woocommerce->mailer();
    $user = wp_get_current_user();
    $reward_points = get_user_meta($user->ID, '_reward_points', true);
    $reward_message = 'Dear ' . $user->first_name . '.<br/>
    <b>You\'ve got money!</b><br/>
    We would like to let you know you have money in your <a href="https://audioplugin.deals/wallet/">APD Rewards Wallet</a>
    which gives a further discount on a wide array of 60+ award-winning products in <a href="https://audioplugin.deals/shop/">The Shop</a>.<br/>
    <center>You have ' . $reward_points . ' in your rewards wallet!</center> <br/><br/>
    Don\'t forget that for every order you make in <a style="color:#d73e1f" href="https://audioplugin.deals/">The Deal</a> section or in <a href="https://audioplugin.deals/shop/">The Shop</a>, you get at least 10% of the amount you spend added to your wallet for future purchases.
    <br/>
    <br/>
    <a href="https://audioplugin.deals/shop/">Shop Now</a>
    ';
    add_filter('woocommerce_email_styles', 'apd_woocommerce_email_styles');
    $image = '<img class="header_logo" src="' . esc_url(get_template_directory_uri()) . '/images/shop/logo_shop.png"/>';

    $msg = $mailer->wrap_message(
        sprintf(__('Audio Plugin Deals - The Shop: Rewards Money Balance'), ''), $reward_message);
    $msg = str_replace('[header_image]', $image, $msg);
    $msg = str_replace('#e78b79', '#2773c9', $msg);
    $msg = str_replace('#d73e1f', '#2773c9', $msg);
    $mailer->send($user->user_email, __('Audio Plugin Deals - The Shop: Rewards Money Balance'), $msg);
}

//add_action('init','apd_customer_apd_report');

function apd_instrument_type_register() {
    register_taxonomy('instrument_type', array('product'), array(
        'labels' => array(
            'name' => 'Instrument Types',
            'singular_name' => 'Instrument Types',
            'search_items' => 'Search Instrument Type',
            'popular_items' => 'Popular Instrument Types',
            'all_items' => 'All Instrument Types',
            'parent_item' => 'Parent Instrument Type',
            'parent_item_colon' => 'Parent Instrument Type:',
            'edit_item' => 'Edit Instrument Type',
            'update_item' => 'Update Instrument Type',
            'add_new_item' => 'Add New Instrument Type',
            'new_item_name' => 'New Instrument Type',
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'instrument_type'),
    ));
    flush_rewrite_rules();
}

add_action('widgets_init', 'apd_instrument_type_register');

function apd_format_type_register() {
    register_taxonomy('format_type', array('product'), array(
        'labels' => array(
            'name' => 'Format Types',
            'singular_name' => 'Format Types',
            'search_items' => 'Search Format Type',
            'popular_items' => 'Popular Format Types',
            'all_items' => 'All Format Types',
            'parent_item' => 'Parent Format Type',
            'parent_item_colon' => 'Parent Format Type:',
            'edit_item' => 'Edit Format Type',
            'update_item' => 'Update Format Type',
            'add_new_item' => 'Add New Format Type',
            'new_item_name' => 'New Format Type',
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'format_type'),
    ));
    flush_rewrite_rules();
}

add_action('widgets_init', 'apd_format_type_register');

function apd_cart_count_fragments($fragments) {
    ob_start();
    ?>
    <a class="wcmenucart-contents" href="<?php echo wc_get_cart_url(); ?>" title="Start shopping">Cart
        (<?php echo WC()->cart->get_cart_contents_count(); ?>)</a>
    <?php
    $fragments['a.wcmenucart-contents'] = ob_get_clean();

    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'apd_cart_count_fragments', 10, 1);

function apd_rewards_update($fragments) {
    $current_user = wp_get_current_user();
    if ($current_user->ID) {
        $reward_points = (get_user_meta($current_user->ID, '_reward_points', true)) ? get_user_meta($current_user->ID, '_reward_points', true) : 0;
    }
    ob_start();
    ?>
    <a href="<?php echo get_permalink(get_page_by_path('wallet')); ?>" class="your_rewards">Your rewards:
        $<?php echo number_format($reward_points, 2, '.', ''); ?></a>
    <?php
    $fragments['a.your_rewards'] = ob_get_clean();

    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'apd_rewards_update', 10, 1);

function apd_process_order_email($classes) {
    include(get_stylesheet_directory() . '/inc/class.ShopOrderEmail.php');

    $classes['WC_Apd_Shop_Order_Email'] = new ApdShopOrderEmail();

    return $classes;
}

add_filter('woocommerce_email_classes', 'apd_process_order_email');

function apd_process_big_order_email($classes) {
    include(get_stylesheet_directory() . '/inc/class.BigDealOrderEmail.php');

    $classes['WC_Apd_BigDeal_Order_Email'] = new ApdBigDealOrderEmail();

    return $classes;
}

add_filter('woocommerce_email_classes', 'apd_process_big_order_email');

function apd_myaccount_label_change($items, $args) {
    if (!is_user_logged_in() && $args->menu == 'Account Menu') {
        $items = str_replace("My Account", "Login", $items);
    }
    $items = str_replace('id="menu-item-204275"', 'data-icon="account" id="menu-item-204275"', $items);

    return $items;
}

add_filter('wp_nav_menu_items', 'apd_myaccount_label_change', 10, 2);

function apd_product_id_callback($post) {
    $apd_stored_meta = (get_post_meta($post->ID, 'meta_product_id', true)) ? get_post_meta($post->ID, 'meta_product_id', true) : false;
    $params = array(
        'posts_per_page' => -1,
        'post_type' => 'product',
        'stock' => 1,
        'orderby' => 'date',
        'order' => 'DESC'
    );
    $wc_query = new WP_Query($params);
    ?>
    <p>
        <label for="meta-text" class="prfx-row-title"><?php _e('Select a product from the list', 'apd') ?></label>
        <select name="meta_product_id">
            <option>Select a product</option>
            <?php
            foreach ($wc_query->posts as $product):
                $selected = ($apd_stored_meta && $apd_stored_meta == $product->ID) ? 'selected' : '';
                echo '<option value="' . $product->ID . '" ' . $selected . '>' . $product->post_title . '</option>';
            endforeach;
            ?>
        </select>
    </p>
    <?php
}

function apd_metabox_callback($post) {
    $apd_stored_meta = get_post_meta($post->ID, 'is_shop_page', true);
    ?>
    <p>
        <label for="meta-text" class="prfx-row-title"><?php _e('Is Shop Page: ', 'apd') ?></label>
        <input type="checkbox" name="is_shop_page" <?php echo ($apd_stored_meta == 'yes') ? 'checked' : ''; ?>>
    </p>
    <?php
}

function apd_product_email_contents($post) {
	$email_template = get_post_meta($post->ID, 'apd_product_email_template', true);
	$email_heading = get_post_meta($post->ID, 'apd_product_email_heading', true);
	?>
	
	<label style="margin: 10px 0px; font-weight: bold; display: block;"><?php _e('Shop Product email subject:', 'apd') ?></label>
	<div><input type="text" name="apd_product_email_heading" size="50" value="<?php echo $email_heading; ?>"></div>
	
	<label  style="margin: 10px 0px; font-weight: bold; display: block;" for="meta_email_template" class="prfx-row-title"><?php _e('Shop Product email template:', 'apd') ?></label>
  <?php echo wp_editor($email_template, 'apd_product_email_template', array('textarea_name' => 'apd_product_email_template')); ?>
	
	<?php
	$deal_email_template = get_post_meta($post->ID, 'apd_deal_product_email_template', true);
	$deal_email_heading = get_post_meta($post->ID, 'apd_deal_product_email_heading', true);
	?>
	<label style="margin: 10px 0px; font-weight: bold; display: block;"><?php _e('Deal Product email subject:', 'apd') ?></label>
	<div><input type="text" name="apd_deal_product_email_heading"  size="50" value="<?php echo $deal_email_heading; ?>"></div>
	
	<label style="margin: 10px 0px; font-weight: bold; display: block;"  for="meta_email_template" class="prfx-row-title"><?php _e('Deal Product email template:', 'apd') ?></label>
  <?php echo wp_editor($deal_email_template, 'apd_deal_product_email_template', array('textarea_name' => 'apd_deal_product_email_template')); ?>
	<?php
}

function apd_product_meta_boxes() {

    add_meta_box('product_email_contents', 'Product Emails', 'apd_product_email_contents', 'product', 'normal', 'high');

    add_meta_box('apd_shop_product_id_page_meta', __('Select a parent product', 'apd'), 'apd_product_id_callback', 'page', 'side');

    add_meta_box('apd_shop_product_page_meta', __('Is Shop Product', 'apd'), 'apd_metabox_callback', 'page', 'side');
}

add_action('add_meta_boxes', 'apd_product_meta_boxes');

function apd_meta_save($post_id) {
    if (isset($_POST['is_shop_page'])) {
        update_post_meta($post_id, 'is_shop_page', 'yes');
    } else {
        update_post_meta($post_id, 'is_shop_page', 'no');
    }

    if (isset($_POST['meta_product_id'])) {
        update_post_meta($post_id, 'meta_product_id', $_POST['meta_product_id']);
    }

    if (isset($_POST['apd_product_email_template'])) {
        update_post_meta($post_id, 'apd_product_email_template', $_POST['apd_product_email_template']);
    }

    if (isset($_POST['apd_product_email_heading'])) {
        update_post_meta($post_id, 'apd_product_email_heading', $_POST['apd_product_email_heading']);
    }
		
		if (isset($_POST['apd_deal_product_email_template'])) {
        update_post_meta($post_id, 'apd_deal_product_email_template', $_POST['apd_deal_product_email_template']);
    }

    if (isset($_POST['apd_deal_product_email_heading'])) {
        update_post_meta($post_id, 'apd_deal_product_email_heading', $_POST['apd_deal_product_email_heading']);
    }
}

add_action('save_post', 'apd_meta_save');

function apd_custom_search($keyword, $paged) {
    $params = array(
        'paged' => $paged,
        'post_type' => 'product',
        'stock' => 1,
        'meta_query' => array(
            array(
                'key' => '_product_type_single',
                'value' => 'yes'
            )
        ),
        's' => $keyword
    );

    return $wc_query = new WP_Query($params);
}

function apd_pre_get_posts_query($q) {
    if (!$q->is_main_query()) {
        return;
    }
    if ($q->is_archive() || get_queried_object()->post_name == 'new-arrivals' || get_queried_object()->post_name == 'trending' || get_queried_object()->post_name == 'search') {
        $q->set('meta_key', '_product_type_single');
        $q->set('meta_value', 'yes');
    }
}

add_action('woocommerce_product_query', 'apd_pre_get_posts_query');

function apd_temporary_rewards_addon() {
    add_management_page('Add rewards', 'Add rewards', 'manage_options', 'temp_rewards', 'apd_add_rewards');
}

add_action('admin_menu', 'apd_temporary_rewards_addon');

function apd_add_rewards() {
    if (isset($_POST['add_temp_rewards'])) {
//        $customers = get_users();
//        if (count($customers) > 0) {
//            foreach ($customers as $customer) {
//                $current_points = (!empty(get_user_meta($customer->ID, '_reward_points', true))) ? get_user_meta($customer->ID, '_reward_points', true) : 0;
//                update_user_meta($customer->ID, '_reward_points', $current_points + $_POST['temp_rewards']);
//            }
//        }
        global $wpdb;
        $wpdb->query($wpdb->prepare("UPDATE `{$wpdb->usermeta}` SET `meta_value` = CAST(`meta_value` AS UNSIGNED) + %d WHERE `meta_key`='_reward_points'", intval($_POST['temp_rewards'])));
        // var_dump($wpdb->last_query, $wpdb->last_error);
    }
    ?>
    <div class="wrap">
        <h1>Add rewards for time period</h1>
        <form action="" method="POST">
            <p><label>Enter the points that should be applied</label></p>
            <p><input type="number" name="temp_rewards"></p>
            <p><input type="submit" name="add_temp_rewards" class="button"></p>
        </form>
    </div>
    <?php
}

function remove_temp_points() {
    $today = strtotime(date('Y-m-d H:i'));
    $expiration_date = strtotime(get_option('temp_rewards_period', true));
    if ($expiration_date && $today >= $expiration_date) {
        $customers = get_users();
        if (count($customers) > 0) {
            foreach ($customers as $customer) {
                $temp_points = (get_user_meta($customer->ID, 'temp_reward_points', true)) ? get_user_meta($customer->ID, 'temp_reward_points', true) : 0;
                if ($temp_points > 0) {
                    $current_points = get_user_meta($customer->ID, '_reward_points', true);
                    update_user_meta($customer->ID, 'temp_reward_points', 0);
                    update_user_meta($customer->ID, '_reward_points', $current_points - $temp_points);
                }
            }
            update_option('temp_rewards_period', 0);
            update_option('temp_rewards_points', 0);
        }
    }
}

add_action('remove_temp_points_hook', 'remove_temp_points');

function apd_rewards_ajax() {
    $current_user = wp_get_current_user();
    if ($current_user->ID) {
        $reward_points = (get_user_meta($current_user->ID, '_reward_points', true)) ? get_user_meta($current_user->ID, '_reward_points', true) : 0;
    }
    ob_start();
    ?>
    Your rewards: $<?php echo number_format($reward_points, 2, '.', ''); ?>
    <?php
    $fragments = ob_get_clean();
    echo $fragments;
    die();
}

// add_action('wp_ajax_rewards', 'apd_rewards_ajax');

function apd_resend_ajax() {
    global $woocommerce;
    if (isset($_POST['orderid'])) {
        $mailer = $woocommerce->mailer();
        $header = "Content-Type: text/html\r\n";
        $order = wc_get_order($_POST['orderid']);

        $recipient = $order->get_billing_email();
        foreach ($order->get_items() as $item) {
            if (get_post_meta($item['product_id'], '_product_type_single', true) == 'yes') {
                $template = get_post_meta($_POST['orderid'], 'email_template_content_' . $item['product_id'], true);
                $subject = "Download instruction for " . $item['name'];
                add_filter('woocommerce_email_styles', 'apd_woocommerce_email_styles');
                $template = str_replace('#e78b79', '#2773c9', $template);
                $template = str_replace('#d73e1f', '#2773c9', $template);
            } elseif (get_post_meta($_POST['orderid'], 'email_template_content_deal', true)) {
                remove_filter('woocommerce_email_styles', 'apd_woocommerce_email_styles');
                $template = get_post_meta($_POST['orderid'], 'email_template_content_deal', true);
                $subject = "Download instruction for " . $item['name'];
            } else {
                remove_filter('woocommerce_email_styles', 'apd_woocommerce_email_styles');
                $template = get_post_meta($_POST['orderid'], 'email_template_content', true);
                $subject = "Download instruction for " . $item['name'];
            }
            $mailer->send($recipient, $subject, $template, $header);
        }
        echo '1';
    }
    die();
}

add_action('wp_ajax_resend', 'apd_resend_ajax');

function apd_facebook_ajax() {
    $current_user = wp_get_current_user();
    if ($current_user->ID) {
        $reward_points = (get_user_meta($current_user->ID, '_reward_points', true)) ? get_user_meta($current_user->ID, '_reward_points', true) : 0;
        $is_used = (get_user_meta($current_user->ID, 'fb_shared', true) == 'yes') ? get_user_meta($current_user->ID, 'fb_shared', true) : 'no';
        if ($is_used == 'no') {
            $reward_points += 20;
            update_user_meta($current_user->ID, '_reward_points', $reward_points);
            update_user_meta($current_user->ID, 'fb_shared', 'yes');
        }
    }
    die();
}

add_action('wp_ajax_facebook', 'apd_facebook_ajax');

function is_subscribed_youtube() {
    if (!is_user_logged_in()) {
        return true;
    }

    $current_user = wp_get_current_user();
    return get_user_meta($current_user->ID, '_youtube_subscribed', true);
}

function apd_wc_login_redirect($redirect) {
    if (isset($_POST['wp_referrer']) && !empty($_POST['wp_referrer'])) {
        $redirect = esc_url($_POST['wp_referrer']);
    } else {
        $redirect = esc_url(get_permalink(get_page_by_path('checkout')));
    }

    return $redirect;
}

add_filter('woocommerce_login_redirect', 'apd_wc_login_redirect');

function apd_wc_load_persistent_cart($user_login, $user = 0) {
    if (!$user) {
        return;
    }

    $saved_cart = (get_user_meta($user->ID, '_woocommerce_persistent_cart_1', true)) ? get_user_meta($user->ID, '_woocommerce_persistent_cart_1', true) : get_user_meta($user->ID, '_woocommerce_persistent_cart', true);
    if ($saved_cart) {
        $cart = array_merge($saved_cart['cart'], WC()->session->cart);
        WC()->session->cart = $cart;
    }
}

add_action('wp_login', 'apd_wc_load_persistent_cart', 1, 2);

function apd_reorder_account_menu($items) {
    // titles
    $items['orders'] = 'Dashboard';
    $items['edit-address'] = 'Billing Details';
    $items['customer-logout'] = 'Log out';

    // remove
    unset($items['dashboard']);
    unset($items['wc-smart-coupons']);
    return $items;
}

add_filter('woocommerce_account_menu_items', 'apd_reorder_account_menu', 99, 1);

function apd_rewards_orders_endpoint($url, $endpoint, $value, $permalink) {
    if ($endpoint === 'orders') {
        $url = get_permalink(get_page_by_path('wallet'));
    }

    return $url;
}

add_filter('woocommerce_get_endpoint_url', 'apd_rewards_orders_endpoint', 10, 4);

function apd_archive_sort_order($query) {
    if ($query->is_archive() && $query->is_main_query()) {
        $category = get_queried_object();
        if ($category->slug == 'blue-light-specials') {
            $query->query_vars['tax_query'] = array(
                array(
                    'taxonomy' => 'product_cat',
                    'terms' => $category->term_id,
                    'operator' => 'IN'
                ),
            );
            $query->query_vars['meta_query'] = array(
                'relation' => 'AND',
                array(
                    'key' => '_product_type_single',
                    'value' => 'yes',
                    'compare' => '=',
                ),
                'sort' => array(
                    'key' => '_blue_light_sort',
                    'compare' => 'EXISTS',
                )
            );
            $query->query_vars['orderby'] = array('sort' => 'ASC', 'date' => 'DESC');
        }

        if ($category->slug == 'gift-certificate') {
            $query->query_vars['tax_query'] = array(
                array(
                    'taxonomy' => 'product_cat',
                    'terms' => $category->term_id,
                    'operator' => 'IN'
                ),
            );
            $query->query_vars['meta_query'] = array(
                'relation' => 'AND',
                array(
                    'key' => '_product_type_single',
                    'value' => 'yes',
                    'compare' => '=',
                ),
                'sort' => array(
                    'key' => '_gift_certificates_sort',
                    'compare' => 'EXISTS',
                ),
            );
            $query->query_vars['orderby'] = array('sort' => 'ASC');
        }
    }
}

add_action('pre_get_posts', 'apd_archive_sort_order');

function apd_remove_from_wish() {
    file_put_contents('/tmp/test.txt', print_r($_REQUEST, true));
    if (isset($_GET['remove_from_wishlist_after_add_to_cart'])) {
        file_put_contents('/tmp/test1.txt', 'dfddd');
        global $wpdb;
        $prod_id = $_REQUEST['remove_from_wishlist_after_add_to_cart'];
        $wishlist_id = $_REQUEST['wishlist_id'];
        $result = $wpdb->delete('wp_yith_wcwl', array('prod_id' => $prod_id, 'wishlist_id' => $wishlist_id));
    }
}

add_action('woocommerce_add_to_cart', 'apd_remove_from_wish');

function apd_shop_order_success_email_template($order_id, $product_id) {
    $email_template_content = get_post_meta($order_id, 'email_template_content_' . $product_id, 'true');

    return $email_template_content;
}

add_filter('woocommerce_continue_shopping_redirect', 'apd_changed_woocommerce_continue_shopping_redirect', 10, 1);
function apd_changed_woocommerce_continue_shopping_redirect($return_to) {
    $return_to = get_permalink(get_page_by_path('shop'));

    return $return_to;
}

add_action('woocommerce_email_order_details', 'apd_order_email', 10, 4);

function apd_order_email($order, $sent_to_admin, $plain_text, $email) {
    if ($sent_to_admin) {
        if (get_post_meta($order->get_id(), 'order_type', true) == 'shop') {
            add_filter('woocommerce_email_styles', 'apd_woocommerce_email_styles');
            $image = '<img class="header_logo" src="' . esc_url(get_template_directory_uri()) . '/images/shop/logo_shop.png"/>';
            $email->template_html = str_replace('#e78b79', '#2773c9', $email->template_html);
            $email->template_html = str_replace('#d73e1f', '#2773c9', $email->template_html);
            $email->template_html = str_replace('[header_image]', $image, $email->template_html);
        } else {
            remove_filter('woocommerce_email_styles', 'apd_woocommerce_email_styles');
            $image = '<img class="header_logo" src="' . esc_url(get_template_directory_uri()) . '/images/logo.png"/>';
            $email->template_html = str_replace('[header_image]', $image, $email->template_html);
        }
    }
}

add_action('woocommerce_payment_complete', 'apd_payment_complete');

function apd_payment_complete($order_id) {
    global $woocommerce;
    $woocommerce->cart->empty_cart();
    $order = new WC_Order($order_id);
    $order->update_status('completed');
}

add_action('woocommerce_thankyou', 'apd_thankyou_hook');

function apd_thankyou_hook() {
    global $woocommerce;
    $woocommerce->cart->empty_cart();
}

function create_post_type() {
    register_post_type('apd_hero_banner',
        array(
            'labels' => array(
                'name' => __('Banner'),
                'singular_name' => __('Banner')
            ),
            'public' => true,
            'has_archive' => false,
        )
    );
}

add_action('init', 'create_post_type');

add_action('user_register', 'apd_registration_bonus', 10, 1);

function apd_registration_bonus($user_id) {
    update_user_meta($user_id, '_reward_points', '20');
}

// slider_custom_post_field

function create_post_your_post() {


    register_post_type( 'your_post',
        array(
            'labels'       => array(
'name'       => __( 'Slider' ),
            ),
            'public'       => true,
            'hierarchical' => true,
            'has_archive'  => true,
            'supports'     => array(
'title',
'editor',
'excerpt',
'thumbnail',
            ),
            'taxonomies'   => array(
'post_tag',
'category',
            )
        )
    );
    register_taxonomy_for_object_type( 'category', 'your_post' );
    register_taxonomy_for_object_type( 'post_tag', 'your_post' );
}

add_action( 'init', 'create_post_your_post' );



// Multiple Add to Cart in URL
function woocommerce_maybe_add_multiple_products_to_cart($url = false) {
    // Make sure WC is installed, and add-to-cart qauery arg exists, and contains at least one comma.
    if (!class_exists('WC_Form_Handler') || empty($_REQUEST['add-to-cart']) || false === strpos($_REQUEST['add-to-cart'], ',')) {
        return;
    }

    // Remove WooCommerce's hook, as it's useless (doesn't handle multiple products).
    remove_action('wp_loaded', array('WC_Form_Handler', 'add_to_cart_action'), 20);

    $product_ids = explode(',', $_REQUEST['add-to-cart']);
    $count = count($product_ids);
    $number = 0;

    foreach ($product_ids as $id_and_quantity) {
        // Check for quantities defined in curie notation (<product_id>:<product_quantity>)
        // https://dsgnwrks.pro/snippets/woocommerce-allow-adding-multiple-products-to-the-cart-via-the-add-to-cart-query-string/#comment-12236
        $id_and_quantity = explode(':', $id_and_quantity);
        $product_id = $id_and_quantity[0];

        $_REQUEST['quantity'] = !empty($id_and_quantity[1]) ? absint($id_and_quantity[1]) : 1;

        if (++$number === $count) {
            // Ok, final item, let's send it back to woocommerce's add_to_cart_action method for handling.
            $_REQUEST['add-to-cart'] = $product_id;

            return WC_Form_Handler::add_to_cart_action($url);
        }

        $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($product_id));
        $was_added_to_cart = false;
        $adding_to_cart = wc_get_product($product_id);

        if (!$adding_to_cart) {
            continue;
        }

        $add_to_cart_handler = apply_filters('woocommerce_add_to_cart_handler', $adding_to_cart->get_type(), $adding_to_cart);

        // Variable product handling
        if ('variable' === $add_to_cart_handler) {
            woo_hack_invoke_private_method('WC_Form_Handler', 'add_to_cart_handler_variable', $product_id);
            // Grouped Products
        } elseif ('grouped' === $add_to_cart_handler) {
            woo_hack_invoke_private_method('WC_Form_Handler', 'add_to_cart_handler_grouped', $product_id);
            // Custom Handler
        } elseif (has_action('woocommerce_add_to_cart_handler_' . $add_to_cart_handler)) {
            do_action('woocommerce_add_to_cart_handler_' . $add_to_cart_handler, $url);
            // Simple Products
        } else {
            woo_hack_invoke_private_method('WC_Form_Handler', 'add_to_cart_handler_simple', $product_id);
        }
    }
}

// Fire before the WC_Form_Handler::add_to_cart_action callback.
add_action('wp_loaded', 'woocommerce_maybe_add_multiple_products_to_cart', 15);

/**
 * Invoke class private method
 *
 * @param string $class_name
 * @param string $methodName
 *
 * @return  mixed
 * @since   0.1.0
 *
 */
function woo_hack_invoke_private_method($class_name, $methodName) {
    if (version_compare(phpversion(), '5.3', '<')) {
        throw new Exception('PHP version does not support ReflectionClass::setAccessible()', __LINE__);
    }

    $args = func_get_args();
    unset($args[0], $args[1]);
    $reflection = new ReflectionClass($class_name);
    $method = $reflection->getMethod($methodName);
    $method->setAccessible(true);

    $args = array_merge(array($class_name), $args);
    return call_user_func_array(array($method, 'invoke'), $args);
}

/** function force_clear_woocommerce_cart()
 * {
 * $user_ID = get_current_user_id();
 * if ($user_ID === 11368)
 * {
 * error_log("Clearing cart");
 * global $woocommerce;
 * $woocommerce->cart->empty_cart();
 * }
 * }
 * add_action( 'init', 'force_clear_woocommerce_cart' ); **/
function wc_save_account_details_required_fields($required_fields) {
    unset($required_fields['account_display_name']);
    return $required_fields;
}

add_filter('woocommerce_save_account_details_required_fields', 'wc_save_account_details_required_fields', 99999);

/* Defer g of Javascript */
function defer_parsing_of_js($url) {
    if (FALSE === strpos($url, '.js')) return $url;
    if (strpos($url, 'jquery.js')) return $url;
    return "$url' defer";
}

// add_filter('clean_url', 'defer_parsing_of_js', 11, 1);
// Changing excerpt more
function new_excerpt_more($more) {
    global $post;
    return '... <a href="' . get_permalink($post->ID) . '">' . 'Read More &raquo;' . '</a>';
}

add_filter('excerpt_more', 'new_excerpt_more');
// Customer excerpt length
function ld_custom_excerpt_length($length) {
    return 50;
}

add_filter('excerpt_length', 'ld_custom_excerpt_length', 999);
// Remove the last link in breadcrumbs
// WHY? Because it an span tag that contains the title of the page
// But you're already on the page, so what's the point?
add_filter('wpseo_breadcrumb_links', 'jj_wpseo_breadcrumb_links');
function jj_wpseo_breadcrumb_links($links) {
    //pk_print( sizeof($links) );
    if (sizeof($links) > 1) {
        array_pop($links);
    }
    return $links;
}

// Add link to the last item in the breadcrumbs
// WHY? Because, by default, WP-SEO doesn't include the link on the last item
// Since we removed in the function above, we need to add the link back in.
add_filter('wpseo_breadcrumb_single_link', 'jj_link_to_last_crumb', 10, 2);
function jj_link_to_last_crumb($output, $crumb) {
    $output = '<a property="v:title" rel="v:url" href="' . $crumb['url'] . '" >';
    $output .= $crumb['text'];
    $output .= '</a>';
    return $output;
}

// Disable repeat purchases of Memory-V
function sv_disable_repeat_purchase($purchasable, $product) {
    // Enter the ID of the product that shouldn't be purchased again
    $non_purchasable = 1220722;

    // Get the ID for the current product (passed in)
    $product_id = $product->is_type('variation') ? $product->variation_id : $product->id;

    // Bail unless the ID is equal to our desired non-purchasable product
    if ($non_purchasable != $product_id) {
        return $purchasable;
    }

    // return false if the customer has bought the product
    if (wc_customer_bought_product(wp_get_current_user()->user_email, get_current_user_id(), $product_id)) {
        $purchasable = false;
    }

    // Double-check for variations: if parent is not purchasable, then variation is not
    if ($purchasable && $product->is_type('variation')) {
        $purchasable = $product->parent->is_purchasable();
    }

    return $purchasable;
}

add_filter('woocommerce_variation_is_purchasable', 'sv_disable_repeat_purchase', 10, 2);
add_filter('woocommerce_is_purchasable', 'sv_disable_repeat_purchase', 10, 2);

function tutsplus_widgets_init() {
    // First footer widget area, located in the footer. Empty by default.
    register_sidebar(array(
        'name' => __('First Footer Widget Area', 'tutsplus'),
        'id' => 'first-footer-widget-area',
        'description' => __('The first footer widget area', 'tutsplus'),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Second Footer Widget Area, located in the footer. Empty by default.
    register_sidebar(array(
        'name' => __('Second Footer Widget Area', 'tutsplus'),
        'id' => 'second-footer-widget-area',
        'description' => __('The second footer widget area', 'tutsplus'),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Third Footer Widget Area, located in the footer. Empty by default.
    register_sidebar(array(
        'name' => __('Third Footer Widget Area', 'tutsplus'),
        'id' => 'third-footer-widget-area',
        'description' => __('The third footer widget area', 'tutsplus'),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Fourth Footer Widget Area, located in the footer. Empty by default.
    register_sidebar(array(
        'name' => __('Fourth Footer Widget Area', 'tutsplus'),
        'id' => 'fourth-footer-widget-area',
        'description' => __('The fourth footer widget area', 'tutsplus'),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

// Register sidebars by running tutsplus_widgets_init() on the widgets_init hook.
add_action('widgets_init', 'tutsplus_widgets_init');

/*
// Owl Js Add

function owl_carousel_file() {
    $aa = rand(10, 10000);
    wp_enqueue_style('owl_carousel_min_css', get_template_directory_uri() . '/css/owl.carousel.min.css');
    wp_enqueue_style('owl_carousel_min_theme_css', get_template_directory_uri() . '/css/owl.theme.default.min.css');
    wp_enqueue_style('slick_css', get_template_directory_uri() . '/js/slick.css');
    wp_enqueue_style('slick_theme_css', get_template_directory_uri() . '/js/slick-theme.css');
    wp_enqueue_script('owl_carousel_min', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), null, true);
    wp_enqueue_script('slick', get_template_directory_uri() . '/js/slick.js', array(), null, true);
    wp_enqueue_script('custom_js', get_template_directory_uri() . '/custom.js', array(), $aa, true);
}

add_action('wp_enqueue_scripts', 'owl_carousel_file');
 * 
 */
?>
<?php
add_action('save_post', 'custom_update_percentage_price');
function custom_update_percentage_price($post_id) {
    if ($_POST['post_type'] != 'product')
        return false;

    if (isset($_POST['_minumum_price']) && isset($_POST['_regular_price'])) {
        $m_price = $_POST['_minumum_price'];
        $r_price = $_POST['_regular_price'];

        $c_price = round(($r_price - $m_price) / $r_price, 2);

        $percent = $c_price * 100;
        update_post_meta($post_id, '_custom_percentage_price', $percent);
    }
}

function wpb_total_posts() {
    $total = wp_count_posts()->publish;
    return $total;
}

add_shortcode('total_posts', 'wpb_total_posts');

// carbonfields
function crb_load() {
    require_once(get_theme_file_path('/vendor/autoload.php'));
    \Carbon_Fields\Carbon_Fields::boot();
}

add_action('after_setup_theme', 'crb_load');

function crb_attach_theme_options() {
    require_once(get_theme_file_path('/inc/carbonfields/bf2019.php'));
    require_once(get_theme_file_path('/inc/carbonfields/theme-options.php'));
}

add_action('carbon_fields_register_fields', 'crb_attach_theme_options');

// youtube subscription
require_once(get_theme_file_path('/inc/youtube-subscription.php'));

// rating system
require_once(get_theme_file_path('/inc/rating-system.php'));

//[custom_post_slider]
function foobar_func( $atts ){
    global $wp_post_types;

    $args = array(
        'post_type'       => 'your_post',
        'posts_per_page'  => -1,
    );
    query_posts( $args );

    ?>
<div class="slideshow-container">
<?php

        while ( have_posts() ) : the_post(); ?>
            
        <div class="sldier_section">
            <div class="slider_content" style="background: url(https://staging.audioplugin.deals/wp-content/uploads/2019/03/bg-memory-v-100off-624x596.png); background-repeat: no-repeat;">
                <div class="slider-left">
                    <?php

                    $image = get_field('image');
                    if( !empty( $image ) ): ?>
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    <?php endif; ?>
                </div>
                <div class="slider-content">
                    <?php
                    $image_two = get_field('image_two');
                    if( !empty( $image_two ) ): ?>
                        <img src="<?php echo esc_url($image_two['url']); ?>" alt="<?php echo esc_attr($image_two['alt']); ?>" />
                    <?php endif; ?>
                </div>
                <div class="slider-right">
                    <p class="slider-des">
                    <?php
                    $text = get_field('text');
                    if( !empty( $text ) ): ?>
                        <?php echo ($text); ?>
                    <?php endif; ?>
                    </p>
                    <div class="slider_btn vc_btn3-container download-for-free-btn vc_btn3-inline">
                    <!--     <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-modern vc_btn3-color-default form-btn" title="DOWNLOAD FOR FREE!" data-sumome-listbuilder-id="f3f1db7d-cbf6-4033-bc3e-7482f0afe06a">DOWNLOAD FOR FREE!</a> -->
                         <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-modern vc_btn3-color-default form-btn" data-toggle="modal" data-target="#myModal">DOWNLOAD FOR FREE!</a>
                    </div>
                </div>
                <div class="modal-section">
                    <form method="post" role="form" action="/">
                        <p class="modal-header-des"><?php $modal_header = get_field('modal_header'); echo ($modal_header);?></p>
                        <div class="form-des">
                            <p class="modal-content-des"><?php $modal_des = get_field('modal_des'); echo ($modal_des);?></p>
                            <input type="hidden" name="action" value="process_form">
                            <input type="hidden" name="redirect_url" value ="<?php $redirect_url = get_field('redirect_url'); echo($redirect_url); ?>">
                            <input class="form-control" type="email" required="" id="email" name="email" value="<?php  if(!empty($email)) echo $email; ?>" 
                                placeholder="Enter your Email" oninvalid="this.setCustomValidity('Please provide a valid email address')">
                            </input>
                            <!-- <input type="email" class="form-control" id="email" name="email" value="<?php  if(!empty($email)) echo $email; ?>" placeholder="Enter your Email" required /> -->
                            <button type="submit" name="submit" class="form-submit"><?php $modal_btn_text = get_field('modal_btn_text'); echo ($modal_btn_text);?></button>   
                            <button class="form-btn-des close" data-dismiss="modal"><?php $modal_bot_des = get_field('modal_bot_des'); echo ($modal_bot_des);?></button>   
                        </div>
                    
                        <div class="form-image">
                             <?php
                                $modal_image = get_field('modal_image');?>
                                 <img src="<?php echo esc_url($modal_image['url']); ?>" alt="<?php echo esc_attr($modal_image['alt']); ?>" />               
                        </div>

                    </form>
                </div>
            </div>
            
        </div>


        <?php endwhile;
        ?>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog download-modal">
        <div class="modal-top"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
        <div class="modal-content">
            <div class="modal-body">
                
            </div>
        </div>
          
        </div>

      </div>
    </div>
 <script type="text/javascript">
        jQuery(document).on('ready', function() {
          jQuery(".slideshow-container").slick({
            lazyLoad: 'ondemand', // ondemand progressive anticipated
            infinite: true,
            autoplay: true,
            dots: true
          });
        });
        jQuery(document).on('ready', function(){
            jQuery(".form-btn").click(function(){
                jQuery('.modal-body').html(jQuery(this).parents('.sldier_section').find('.modal-section').html());
            });

        });
</script>

    </body>
    <?php

}

add_shortcode( 'custom_post_slider', 'foobar_func' );
