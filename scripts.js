/**
 * Here we place JS scripts that are used globally on the site.
 * Other specific scripts are located in separate files/
 * 
 * 
 * The theme loads scripts.compiled.js which contains all JS scripts combined into one file by Grunt.
 */

const JS_VERSION = '0.1';