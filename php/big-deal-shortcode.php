<?php 
class Big_Deal_Shortcode {

    public function __construct() {
        add_shortcode('big_deal_items', array($this, 'render_deals_shortcode'));
    }

    public function get_mainpage_bigdeal_items() {
        global $wpdb;
        $wp = $wpdb->prefix;

        $products = array();

        $query_sql = "SELECT p.ID, p.post_title, pm_countdown.meta_value AS 'bigdeal_countdown_date', 
			  ( SELECT pm_page_url.meta_value FROM `{$wp}postmeta` AS pm_page_url 
					  WHERE pm_page_url.post_id = p.ID 
					  AND pm_page_url.meta_key = 'bigdeal_page_url' ) AS bigdeal_page_url,
			  ( SELECT pm_image.meta_value FROM `{$wp}postmeta` AS pm_image 
					  WHERE pm_image.post_id = p.ID 
					  AND pm_image.meta_key = 'image_for_mainpage_banner' ) AS mainpage_deal_image,
			  ( SELECT pm_discount.meta_value FROM `{$wp}postmeta` AS pm_discount 
					  WHERE pm_discount.post_id = p.ID 
					  AND pm_discount.meta_key = 'deal_discount_percent' ) AS discount_percent
		FROM {$wp}posts AS p 		 
		LEFT JOIN `{$wp}postmeta` AS pm on p.`ID` = pm.`post_id`
		LEFT JOIN `{$wp}postmeta` AS pm_enabled on p.`ID` = pm_enabled.`post_id`
		LEFT JOIN `{$wp}postmeta` AS pm_countdown on p.`ID` = pm_countdown.`post_id`
		WHERE pm.meta_key = '_product_big_deal' AND pm.meta_value = 'yes' 
		AND pm_enabled.meta_key = 'ubermenu_item_enabled' AND pm_enabled.meta_value != ''
		AND pm_countdown.meta_key = 'bigdeal_countdown_date' AND pm_countdown.meta_value != ''
		AND p.post_type = 'product' AND p.post_status = 'publish' 
		ORDER BY pm_countdown.meta_value";

        $product_data = $wpdb->get_results($query_sql, ARRAY_A);

        foreach ($product_data as $row) {

            $product_id = $row['ID'];

            $product_image = wp_get_attachment_image_src($row['mainpage_deal_image'], 'full');
            $wc_product = wc_get_product($product_id);
						$developers = get_the_terms($product_id, 'developer' ); // get all developers

            $products[$product_id] = array(
                'title' => $row['post_title'],
                'image_src' => $product_image[0],
                'end_date' => $row['bigdeal_countdown_date'],
                'price' => $wc_product->get_price(),
                'discount' => $row['discount_percent'],
								'developer' => array_pop(wp_list_pluck($developers, 'name')) // get the first dev name
            );

            if ($row['bigdeal_page_url']) {
                $products[$product_id]['url'] = $row['bigdeal_page_url'];
            } else {
                $products[$product_id]['url'] = get_permalink($product_id);
            }
        }

        return $products;
    }

    public function render_deals_shortcode() {
        $out = '<div class="mp-deals-wrapper">';

        $items = $this->get_mainpage_bigdeal_items();

        $i = 0;
        foreach ($items as $product_id => $item) {

            $date_diff = strtotime($item['end_date']) - time();
						$countdown_end_date = date('M j, Y 00:00:00', strtotime($item['end_date']));// Aug 20, 2022 00:00:00
						
						if ( $item['discount'] < 100 && $item['discount'] > 0 ) {
							$percent_class = "discount_50";
							if ( $item['discount'] > 50 ) { $percent_class = 'discount_70'; }
							if ( $item['discount'] > 70 ) { $percent_class = 'discount_80'; }
							if ( $item['discount'] > 80 ) { $percent_class = 'discount_90'; }
						}
						else {
							$item['discount'] = false;
						}

            if ( $date_diff > 0 ) {

                $i++;
                $days = floor($date_diff / 86400);
                $hours = floor(( $date_diff - ($days * 86400) ) / 3600);
                $minutes = floor(( $date_diff - ($days * 86400) - ($hours * 3600) ) / 60);
                $seconds = rand(10, 59);


                $item_video = get_field("video_url_for_mainpage_banner", $product_id);
                $item_audio = get_field("soundcloud_url", $product_id); 

                if ( $item_video ) {
                    $watch_btn = '<a class="video-link" data-featherlight="iframe" id="popup_link_' . $i . '_video" href="' . $item_video . '" data-featherlight-variant="big-deal-video-lightbox" ><i class="fab fa-youtube"></i></a>';
                } else {
                    $watch_btn = '';
                }

                if ( $item_audio ) {
                    $listen_btn = '<a class="sound-link" data-featherlight="iframe" id="popup_link_' . $i . '_sound" href="' . $item_audio . '" data-featherlight-variant="big-deal-sound-lightbox"><i class="fa fa-volume-up"></i></a>';
                } else {
                    $listen_btn = '';
                }

								ob_start();
								?>
								<div class="mp-deal-item">
										<div class="mp-deal-item-inner">
												<div class="mp-deal-top">
														<div class="mp-deal-text-box"><?php echo $listen_btn . $watch_btn ?></div>
														<?php if ( $item['discount'] ): ?>
															<div class="mp-deal-percents-off <?php echo $percent_class; ?>"><?php echo $item['discount']; ?>% OFF</div>
														<?php endif; ?>
												</div>
												
												<a href="<?php echo $item['url']; ?>" target="_self" class="mp-deal-img" rel="noopener">
													<img width="300" height="300" src="<?php echo $item['image_src']; ?>" class="attachment-full" alt="<?php echo $item['title']; ?>" >
												</a>

												<h5 class="mp-deal-developer"><?php echo $item['developer']; ?></h5>
												<h4 class="mp-deal-product-title">
													<a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
												</h4>

												<div class="mp-deal-bottom">
													<div class="mp-deal-info-wrapper">
														<div class="mp-deal-pricebox">
															Get it for 
															<div class="price-container">
																<span class="discount-price">$<?php echo $item['price']; ?></span>
																<span class="original-price">$<?php echo $item['price']; ?></span>
															</div>
														</div>
														<div class="mp-deal-date">
															Deal ending in
															<div class="content-countdown countdown-<?php echo $product_id; ?>">
																<div class="countdown">
																	<span class="digits-container"><span class="days"><?php echo str_pad($days, 2, '0', STR_PAD_LEFT); ?></span><span class="time-divider">:</span></span>
																	<span class="digits-container"><span class="hours"><?php echo str_pad($hours, 2, '0', STR_PAD_LEFT); ?></span><span class="time-divider">:</span></span>
																	<span class="digits-container"><span class="minutes"><?php echo str_pad($minutes, 2, '0', STR_PAD_LEFT); ?></span><span class="time-divider">:</span></span>
																	<span class="digits-container"><span class="seconds"><?php echo str_pad($seconds, 2, '0', STR_PAD_LEFT); ?></span><span class="time-divider"></span></span>
																</div>
															</div>
														</div>
													</div>
													<a class="mp-deal-btn" href="/cart/?add-to-cart='<?php echo $product_id; ?>">Add to Cart</a>
												</div>
										</div>
										<script> 
											document.addEventListener(
												'DOMContentLoaded', 
												function(event) { 
													initCountdownTimer( '<?php echo $product_id; ?>','<?php echo $countdown_end_date; ?>' ); 
												}
											);
										</script>
								</div>
								<?php
								
								$item_html = ob_get_contents();
								ob_end_clean();
              
                 
                $out .= $item_html;
                $out .= $item_html;
                //$out .= $item_html;
            }
        }

        $out .= '</div>';
      
        return $out;
    }

}

$big_deal_shortcode = new Big_Deal_Shortcode();