<?php 
class Account_Rewards_Shortcode {

	public function __construct() {
			add_shortcode('account_rewards', array($this, 'render_shortcode'));
	}

	public function get_user_rewards( $user_id ) {
		$rewards = get_user_meta($user_id, '_reward_points', true );
		return $rewards;
	}

	public function render_shortcode() {
		$user_id = get_current_user_id();
		$rewards = 0;

		if ( $user_id ) {
			$rewards = absint( $this->get_user_rewards($user_id) );
		} 

		$out = '<div class="rewards-wrapper">' . $rewards . '</div>';
		return $out;
	}
}

$rewards_shortcode = new Account_Rewards_Shortcode();


class Account_Savings_Shortcode {

	public function __construct() {
			add_shortcode('account_savings', array($this, 'render_shortcode'));
	}

	public function get_user_savings( $user_id ) {
		$savings = get_user_meta($user_id, '_total_savings', true );
		return $savings;
	}

	public function render_shortcode() {
		$user_id = get_current_user_id();
		$savings = 0;

		if ( $user_id ) {
			$savings = absint( $this->get_user_savings($user_id) );
		} 

		$out = '<div class="savings-wrapper">' . $savings . '</div>';
		return $out;
	}
}

$savings_shortcode = new Account_Savings_Shortcode();


class Account_Earnings_Shortcode {

	public function __construct() {
			add_shortcode('account_earnings', array($this, 'render_shortcode'));
	}

	public function get_user_earnings( $user_id ) {
		$savings = get_user_meta($user_id, '_total_earned', true );
		return $savings;
	}

	public function render_shortcode() {
		$user_id = get_current_user_id();
		$earnings= false;

		if ( $user_id ) {
			$earnings = $this->get_user_earnigns($user_id);
		} 

		$out = '<div class="savings-wrapper">' . $earnings . '</div>';
		return $out;
	}
}

$earnings_shortcode = new Account_Earnings_Shortcode();


class Account_Item_Stats_Shortcode {

	public function __construct() {
			add_shortcode('account_items_count', array($this, 'render_shortcode'));
	}

	public function get_user_stat_value( $user_id, $key = 'deals' ) {
		$user_stats = get_user_meta($user_id, '_user_stats', true );
		$value = 0;

		if ( is_array( $user_stats ) ) {
			if ( isset( $user_stats[$key] ) ) {
				$value = $user_stats[$key];
			}
		}

		return $value;
	}

	public function render_shortcode( $atts ) {
		$atts = shortcode_atts( array(
				'display' => 'deals',
		), $atts, 'account_items_count' );

		$user_id = get_current_user_id();
		$value = 0;

		if ( $user_id ) {
			$value = $this->get_user_stat_value($user_id, $atts['display']);
		} 

		$out = '<div class="user-stat-wrapper user-stat-' . $atts['display'] . '">' . $value . '</div>';
		return $out;
	}
}

$stats_shortcode = new Account_Item_Stats_Shortcode();



class Account_Recent_Products_Shortcode {

	static $rating_names = array(
			 '0' => '0. Trash',
			 '1' => '1. Horrible',
			 '2' => '2. Bad',
			 '3' => '3. So so',
			 '4' => '4. Great',
			 '5' => '5. Mind Blown!',
	);
	 
	public function __construct() {
			add_shortcode('account_recent_products', array($this, 'render_shortcode'));
	}

	public static function get_product_rating_data($product_id) {
		$rating_count = get_post_meta($product_id, '_apd_rating_count', true);
		$rating_count = is_numeric($rating_count) ? intval($rating_count) : 0;
		$rating_sum = get_post_meta($product_id, '_apd_rating_sum', true);
		$rating_sum = is_numeric($rating_sum) ? doubleval($rating_sum) : 0;

		// average
		$average_rating = $rating_sum / $rating_count;
		$rounded_average_rating = round($average_rating, 0);

		return array(
				'average_rating' => number_format($average_rating, 1, '.', ''),
				'rating_count' => $rating_count,
				'rounded_average_rating' => $rounded_average_rating,
				'rounded_average_rating_text' => self::$rating_names[$rounded_average_rating],
		);
	}

	public function get_user_recent_products( $user_id, $limit = 5 ) {
		
		// first get completed user orders 
		$args = array(
			'paged' => 1,
			'post_type' => wc_get_order_types('view-orders'),
			'post_status' => 'wc-completed',
			'meta_query' => array(
					'relation' => 'AND',
					array(
							'key' => '_customer_user',
							'value' => $user_id,
					),
			),
		);

		$products = [];
		$product_ids = [];
		$orders = get_posts($args);
	
		// after that get products from each order and skip duplicated products 
		foreach ( $orders as $order ) {
			$wc_order = wc_get_order( $order->ID );
			$order_items = $wc_order->get_items(); // array( WC_Order_Item )

			foreach ( $order_items as $item ) {

				$product_id = $item['product_id'];
				
				if ( in_array( $product_id, $product_ids ) ) {
					continue; // skip duplicate
				}
				else {
					$product_ids[] = $product_id;
				}
			
				$earned = 5; // see APD_Rating_System::add_rating_rewards()
				
				$post_thumbnail_id = get_post_thumbnail_id($product_id);
        $full_size_image = wp_get_attachment_image_src($post_thumbnail_id, 'medium');
				
				$rating_data = self::get_product_rating_data($product_id);
				$rating_img = self::get_rating_image_src($rating_data['rounded_average_rating']);
				
				$is_rated = ! APD_Rating_System::current_user_can_rate_product($product_id);
					
				$products[] = array(
						'id'								=> $product_id,
						'title'							=> $item->get_name(),
						'image_src'					=> $full_size_image[0],
						'is_rated'					=> $is_rated,
						'rating_data'				=> $rating_data,
						'rating_img'				=> $rating_img,
						'earned'						=> $earned
				);
				
				if ( count($products) == $limit ) { break; }
			}
		}
		
		return $products;
	}
	
	public static function get_rating_image_src( $product_rating ) {
		if ( intval($product_rating) <= 0 ) {
			$product_rating = 0;
		}
		if ( intval($product_rating) > 5 ) {
			$product_rating = 5;
		}
		
		$img_src =  get_stylesheet_directory_uri() . '/img/rate-' . intval($product_rating) . '.png';
						
		return $img_src;
	}

	public function render_shortcode() {
		$user_id = get_current_user_id();
		$out = '';

		if ( $user_id ) {
			$products = $this->get_user_recent_products( $user_id );

			if ( ! $products ) {
				$out = '<div class="recent-products-wrapper">Once you buy something, you will be able to rate your purchased products here</div>';
			}
			else {
				foreach ( $products as $product ) {
					
					$is_rated = $product['is_rated'];
					$overlay_class = $is_rated ? 'rated-overlay' : 'unrated-overlay';
					
					ob_start();
					?>
					<div class="recent-item">
						<?php if ( ! $is_rated ): ?>
							<a href="#" data-featherlight="#review-target-<?php echo $product['id']?>" >
						<?php endif; ?>
							<div class="recent-item-inner" style="background-image: url('<?php echo $product['image_src']; ?>') ">
								<div class="<?php echo $overlay_class; ?>"></div>
							</div>
							<div class="<?php echo $overlay_class; ?>-text">
								<?php if ( $is_rated ) : ?>
									<div class="rating-image"><img src="<?php echo $product['rating_img'] ?>"/></div>
									<h4><?php echo $product['rating_data']['average_rating']; ?></h4>
									<h5>You earned<br>$&nbsp;<?php echo $product['earned'] ?></h5>
									<?php else: ?>
									<h5>Review to&nbsp;earn<br>$&nbsp;<?php echo $product['earned'] ?></h5>
								<?php endif; ?>
							</div>
						<?php if ( ! $is_rated ): ?>
							</a>
							<div style="display:none">
								<div id="review-target-<?php echo $product['id']?>">
									<?php $this->render_review_popup($product); ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
					<?php

					$product_html = ob_get_contents();
					ob_end_clean();
					$products_html .= $product_html . $product_html;
				}
				$out = '<div class="recent-products-wrapper"><div class="recent-products-inner">' . $products_html . '</div></div>';
			}
			
		} 

		return $out;
	}
	
	private function render_review_popup( $product ) {
		
		$loader_img_src =  get_stylesheet_directory_uri() . '/img/loading.png';
		
		?>
		<div class="rating-system-content rating-<?php echo $product['id']?>">
        <div class="rsc__product-image"><img src="<?php echo $product['image_src']; ?>" /></div>
        <div class="rsc__product-title"><?php echo $product['title']; ?></div>
        <div class="rsc__form">
            <div class="rsc__heading">On a scale of 0 - 5, rate your overall experience with this product.</div>
            <ul class="rsc__rating">
                <?php $rating_options = APD_Rating_System::$option;
                foreach ($rating_options as $value => $label): ?>
                    <li class="rsc__rating__option">
                        <input id="<?php echo $product['id']?>-rating-<?php echo $value; ?>" name="rating" type="radio"
                               value="<?php echo $value; ?>">
                        <label for="<?php echo $product['id']?>-rating-<?php echo $value; ?>"><span><?php echo $label; ?></span></label>
                    </li>
                <?php endforeach; ?>
            </ul>
            <button type="button" class="btn btn-submit-rating" disabled data-product-id="<?php echo $product['id']?>">Submit</button>
						<img class="rsc__loader" style="display:none;height:30px;margin: 20px;" src="<?php echo $loader_img_src; ?>" />
            <div class="rsc__error"></div>
            <div class="rsc__hint">You will immediately receive $5 into your APD rewards wallet after rating this
                product!
            </div>
        </div>
        <div class="rsc__info-and-thank-you">
            <div class="rsc__thank-you-message">Thanks for rating this product! We've just added $5 to your rewards
                wallet.
            </div>
        </div>
    </div>
		<?php
	}
}

$recent_products_shortcode = new Account_Recent_Products_Shortcode();

class Account_Recent_Orders_Shortcode {
	
	const ORDERS_PER_PAGE = 5;
	
	public function __construct() {
		add_shortcode('account_recent_orders', array($this, 'render_shortcode') );
		add_action('wp_ajax_more_recent_orders', array($this, 'ajax_get_more_recent_orders') );
		add_action('wp_ajax_search_orders', array($this, 'ajax_search_orders') );
	}
	
	
	public function find_total_order_count( $user_id ) {
		global $wpdb;
		
		$sql = 'SELECT count(wp_posts.ID) as count FROM wp_posts '
			. ' INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id ) '
			. ' WHERE ( ( wp_postmeta.meta_key = "_customer_user" AND wp_postmeta.meta_value = %d ) ) '
			. ' AND wp_posts.post_type = "shop_order" ' //  IN ("shop_order", "shop_order_refund") 
			. ' AND wp_posts.post_status = "wc-completed" ';

		$result = $wpdb->get_row( $wpdb->prepare($sql, $user_id), ARRAY_A );
		
		return $result['count'];
	}
	
	
	private function find_user_order_ids( $user_id, $starting_order_id = 0 ) {
		global $wpdb;
		
		if ( $starting_order_id > 0 ) {
			$sql = 'SELECT wp_posts.ID as ID FROM wp_posts '
				. ' INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id ) '
				. ' WHERE ( ( wp_postmeta.meta_key = "_customer_user" AND wp_postmeta.meta_value = %d ) ) '
				. ' AND wp_posts.post_type = "shop_order" ' //  IN ("shop_order", "shop_order_refund") 
				. ' AND wp_posts.post_status = "wc-completed" '
				. ' AND wp_posts.ID < %d '
				. ' ORDER BY wp_posts.ID DESC '
				. ' LIMIT %d';
			
			$result = $wpdb->get_results( $wpdb->prepare($sql, $user_id, $starting_order_id, self::ORDERS_PER_PAGE + 1), ARRAY_A );
		}
		else {
			$sql = 'SELECT wp_posts.ID as ID FROM wp_posts '
				. ' INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id ) '
				. ' WHERE ( ( wp_postmeta.meta_key = "_customer_user" AND wp_postmeta.meta_value = %d ) ) '
				. ' AND wp_posts.post_type = "shop_order" ' //  IN ("shop_order", "shop_order_refund") 
				. ' AND wp_posts.post_status = "wc-completed" '
				. ' ORDER BY wp_posts.ID DESC'
				. ' LIMIT %d';
			
			$result = $wpdb->get_results( $wpdb->prepare($sql, $user_id, self::ORDERS_PER_PAGE), ARRAY_A );
		}

		function extract_id($array) {
			return $array['ID'];
		}
		
		$ids = array_map('extract_id', $result);
		
		return $ids;
	}
	
	private function find_user_order_by_id( $user_id, $order_id ) {
		global $wpdb;
		
		$sql = 'SELECT wp_posts.ID as ID FROM wp_posts '
				. ' INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id ) '
				. ' WHERE ( ( wp_postmeta.meta_key = "_customer_user" AND wp_postmeta.meta_value = %d ) ) '
				. ' AND wp_posts.post_type = "shop_order" ' //  IN ("shop_order", "shop_order_refund") 
				. ' AND wp_posts.post_status = "wc-completed" '
				. ' AND wp_posts.ID = %d '
				. ' ORDER BY wp_posts.ID DESC';
			
		$result = $wpdb->get_row( $wpdb->prepare($sql, $user_id, $order_id), ARRAY_A );
		
		if ( $result ) {
			return $result['ID'];
		}
		else {
			return false;
		}
	}
	
	private function find_user_orders_by_product( $user_id, $product_name ) {
		global $wpdb;
		
		$sql = 'SELECT order_id FROM wp_woocommerce_order_items as o_items '
				. ' INNER JOIN wp_postmeta ON ( o_items.order_id = wp_postmeta.post_id ) '
				. ' WHERE ( wp_postmeta.meta_key = "_customer_user" AND wp_postmeta.meta_value = %d ) '
				. ' AND o_items.order_item_type = "line_item" '
				. ' AND o_items.order_item_name LIKE %s '
				. ' ORDER BY order_id DESC';
		
		$result = $wpdb->get_results( $wpdb->prepare($sql, $user_id, '%' . $product_name . '%' ), ARRAY_A );
		
		
		function extract_order_id($array) {
			return $array['order_id'];
		}
		
		$ids = array_map('extract_order_id', $result);
		
		//echo('$result<pre>' . print_r($ids, 1) . '</pre>');
		//die();
		return $ids;
	}
	
	public function find_user_recent_orders( $user_id ) {
		
		$ids = $this->find_user_order_ids( $user_id );
		
		$max_orders = self::ORDERS_PER_PAGE;
		
		$args = array(
			'posts_per_page'				=> $max_orders,
			'post__in'							=> $ids,
			'post_type'							=> wc_get_order_types('view-orders'),
			'post_status'						=> 'wc-completed',
			'ignore_sticky_posts'		=> true,
			'order'									=> 'DESC',
			'order_by'							=> 'ID'
		);
		
		$order_posts = get_posts($args);
		$result = $this->prepare_order_data( $order_posts );
		
		return $result;
	}
	
	private function prepare_order_data( $order_posts, $product_name_filter = '' ) {
		
		$order_page_root = get_permalink(get_page_by_path('my-account'));
		$result = [];
		
		//  get products from each order and skip duplicated products 
		foreach ( $order_posts as $key => $order ) {
			
			$wc_order = wc_get_order( $order->ID );
			$order_items = $wc_order->get_items(); // array( WC_Order_Item )
			$order_meta = get_post_meta( $order->ID );
			
			if ( isset($order_meta['_invoice_number']) ) {
				$invoice_link = $order_page_root . 'orders/?pdfid=' . $order->ID;					
			}
			elseif ( isset($order_meta['fastspring_invoice_url']) ) {
				$invoice_link = $order_meta['fastspring_invoice_url'][0];
			} else {
				$invoice_link = '';
			}

			$result[$key] = array(
				'order_id'			=> $wc_order->ID,
				'order_object'	=> $wc_order,
				'wc_items'			=> [],
				'invoice_link'	=> $invoice_link
			);
			
			foreach ( $order_items as $item ) {
				
				// skip products that are not matching by name (in case of search by product name)
				if ( $product_name_filter != '' ) {
					if ( stripos( $item->get_name(), $product_name_filter ) === false ) {
						continue;
					}
				}
				
				$product_id = $item['product_id'];
				$product = wc_get_product($product_id);

				if ( $product ) {
					$result[$key]['wc_items'][$product_id] = $this->prepare_order_line_data( $item, $product );
				}
				
			}
		}
		
		return $result;
	}
	
	public function prepare_order_line_data( $item, $product ) {
		$result = array(
			'product_id'		=> $product->id,
			'name'					=> $item->get_name(),
			'total'					=> $item->get_total(),
			'wc_item'				=> $item,
			'purchase_note' => get_post_meta( $product->id, '_purchase_note', true )
		);
		
		if ( $product->get_downloadable() ) {
			$result['is_downloadable'] = true;
			$links = $product->get_downloads();
			$result['download_links'] = array();
			
			foreach ( $links as $link ) {
				$file_name = $link->get_name();
				$file_url = do_shortcode($link->get_file());
				$result['download_links'][$file_name] = $file_url;
			}
		}
		else {
			$result['is_downloadable'] = false;
		}

		return $result;
	}
	
	// ajax callback
	public function ajax_get_more_recent_orders() {
		
		$max_orders = self::ORDERS_PER_PAGE;
		$user_id = get_current_user_id();
		$starting_order_id = $_POST['start_from'];
		$ids = $this->find_user_order_ids( $user_id, $starting_order_id );
		
		if ( count($ids) ) {
			$args = array(
				'posts_per_page'				=> $max_orders,
				'post__in'							=> $ids,
				'ignore_sticky_posts'		=> true,
				'post_type'							=> wc_get_order_types('view-orders'),
				'post_status'						=> 'wc-completed',
				'order'									=> 'DESC',
				'order_by'							=> 'ID'
			);

			$order_posts = get_posts($args);
			
			
			$order_data = $this->prepare_order_data( $order_posts );
			
			$last_order = end($order_data); // last order to show to user on this page
			
			
			//echo('$order_data<pre>' . print_r($order_data, 1) . '</pre>');die();
			//echo('end($ids)<pre>' . print_r(end($ids), 1) . '</pre>');
			
			// if last shown order id is more than last found order (they are sorted in decreasing order)
			// it would mean that there are some orders still left to be shown on the next page
			$orders_left = $last_order['order_id'] > end($ids) ? true : false; 
			
			$html = $this->render_ajax_order_lines( $order_data );

			$result = array(
				'found_orders_html' => $html,
				'last_order'				=> $last_order['order_id'],
				'orders_left'				=> $orders_left,
			);
		}
		else {
			$result = array(
				'found_orders_html' => false,
				'last_order'				=> false,
				'orders_left'				=> 0,
			);
		}
	
		//echo('<pre>' . print_r($result, 1) . '</pre>');die();
		wp_send_json_success($result);
		wp_die();
	}
	
	
	// ajax callback
	public function ajax_search_orders() {
		
		$user_id = get_current_user_id();
		$search_string = trim($_POST['s']);
		$found_orders = false;
		
		if ( is_numeric($search_string) ) {
			$order_id = $this->find_user_order_by_id( $user_id, intval($search_string) );
			if ( $order_id ) {
				$order_post = get_post($order_id);
				if ( $order_post ) {
					$order_data = $this->prepare_order_data( array($order_post) );
					$html = $this->render_ajax_order_lines( $order_data );
					$found_orders = true;
				}
			}
		}
		else {
			$order_ids = $this->find_user_orders_by_product( $user_id, $search_string );
			
			if ( count($order_ids) ) {
				$found_orders = true;
				
				$args = array(
					'nopaging' => true,
					'post__in'							=> $order_ids,
					'ignore_sticky_posts'		=> true,
					'post_type'							=> wc_get_order_types('view-orders'),
					'post_status'						=> 'wc-completed',
					'order'									=> 'DESC',
					'order_by'							=> 'ID'
				);
				
				$order_posts = get_posts($args);
				$order_data = $this->prepare_order_data( $order_posts, $search_string );
				$html = $this->render_ajax_order_lines( $order_data );
			}
		}
		
		$result = array( 'found_orders_html' => $found_orders ? $html : false );
				
		//echo('<pre>' . print_r($result, 1) . '</pre>');die();
		wp_send_json_success($result);
		wp_die();
	}
	
	private function render_ajax_order_lines( $orders ) {
		ob_start();
		foreach ( $orders as $order ) {
			if ( count($order['wc_items']) > 0 ) {
				foreach ( $order['wc_items'] as $item ) {
					$this->render_single_product_line( $order, $item );
				}
			}
		}
		$order_lines = ob_get_contents();
		ob_end_clean();

		return $order_lines;
	}

	public function render_shortcode() {
		
		$max_orders = self::ORDERS_PER_PAGE;
		
		$user_id = get_current_user_id();
		$out = '';
		$loader_img_src =  get_stylesheet_directory_uri() . '/img/loading.png';

		if ( $user_id ) {
			$orders = $this->find_user_recent_orders( $user_id );
			$total_orders_count = $this->find_total_order_count( $user_id );
			
			if ( ! $orders ) {
				$out = '<div class="recent-orders-wrapper">Once you buy something, you will be able to view your orders here</div>';
			}
			else {
				
				$last_order_id = 0;
				ob_start();
				?>
				<div class="heading">
					<div class="header-container">
						<h2 id="orders-header">Order history</h2>
					</div>
					<div class="search-container">
							<input type="search" id="order-search" name="order_search" placeholder="Search..." />
							<div class="btn" id="order-search-trigger"><i class="fas fa-search"></i></div>
							<div class="btn" id="search-loader"><img src="<?php echo $loader_img_src; ?>"/></div>
					</div>
				</div>
				<br />
				<div class="content-container">
					<div id="recent-orders">
						<div class="recent-order-rows">
						<?php foreach ( $orders as $order ): ?>
							<?php if ( count($order['wc_items']) > 0 ): ?>
								<?php foreach ( $order['wc_items'] as $item ): ?>
									<?php $this->render_single_product_line( $order, $item ); ?>
								<?php endforeach; ?>
							
								<?php $last_order_id = $order['order_id']; ?>
							<?php endif; ?>
						<?php endforeach; ?>
							</div>
					</div>
					<div id="found-orders">
						<div class="found-order-rows">
						</div>
					</div>
				</div>
				<?php if ( $total_orders_count > $max_orders ) : ?>
					<div class="showmore-container">
						<div class="btn" id="show-more-orders" data-last-order="<?php echo $last_order_id; ?>">Show earlier orders</div>
						<div class="btn" id="show-more-loader"><img src="<?php echo $loader_img_src; ?>"/></div>
						<div class="btn" id="clear-search-btn" style="display:none">Clear search results</div>
					</div>
				<?php endif; ?>
				<?php
				$orders_html .= ob_get_contents();
				ob_end_clean();
				
				$out = '<div class="recent-orders-wrapper">' . $orders_html . '</div></div>';
			}
		}
		
		return $out;
	}
	
	public function render_single_product_line( $order, $item ) {
		
		$wc_order = $order['order_object'];
		$wc_item = $item['wc_item'];

		$show_purchase_note = $wc_order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array(
			'completed',
			'processing'
		) ) );
		?>
		
			<div class="order-name"><?php echo $item['name'];?></div>
			<div class="order-id">#<?php echo $order['order_id']; ?></div>
			<div class="order-total">$<?php echo $item['total']; ?></div>
			
			<?php if ($order['invoice_link']): ?>
				<div class="order-invoice"><a href="<?php echo $order['invoice_link']; ?>">Invoice</a></div>
			<?php else: ?>
				<div class="order-invoice"></div>
			<?php endif; ?>
			<div class="order-link">
				<?php if ( $item['is_downloadable']): ?>
					<?php if ( count($item['download_links']) === 1 ) : ?>
						<?php $file_url = end($item['download_links']); ?>
						<a href="<?php echo $file_url; ?>" target="_blank" >Download</a>
					<?php elseif ( count($item['download_links']) > 1 ) : ?>		
						<a href="#" data-featherlight="#download-contents-<?php echo $item['product_id']; ?>">Download</a>
						<div style="display:none;">
							<div class="download-contents" id="download-contents-<?php echo $item['product_id']; ?>">
								<h4>Download links for <?php echo $item['name'];?></h4>
								<ul class="product-download-links">
									<?php foreach ($item['download_links'] as $file_name => $file_url ): ?>
										<li><a href="<?php echo $file_url; ?>" target="_blank" ><?php echo $file_name; ?></a></li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					<?php endif; ?>
				<?php else: ?>
					<a href="#" data-featherlight="#product-view-<?php echo $item['product_id']; ?>">Instructions</a>
					<div style="display:none;">
						<div class="order-view" id="product-view-<?php echo $item['product_id']; ?>">
							<h4>Order item details</h4>
							<?php
								if ( $wc_item ) {
									wc_get_template( 'order/order-details-item-popup.php', array(
										'order'              => $wc_order,
										'item'               => $wc_item,
										'purchase_note'      => $item['purchase_note'],
										'show_purchase_note' => $show_purchase_note,
									) );
								}
							?>
						</div>
					</div>					
				<?php endif; ?>
			</div>
		
		<?php
	}
}

$recent_orders_shortcode = new Account_Recent_Orders_Shortcode();


class Account_Edit_Details_Form_Shortcode {
	 
	public function __construct() {
			add_shortcode('account_edit_details_form', array($this, 'render_shortcode'));
	}

	public function render_shortcode() {
		$user_id = get_current_user_id();
		$out = '';

		if ( $user_id ) {
			$user = get_user_by( 'id', get_current_user_id());
			
			if ( $user ) {
				ob_start();
			
				if ( isset( $_POST['action']) && ( $_POST['action'] == 'save_account_details') ) {
					$show_notices = true;
				}
				else {
					$show_notices = false;
				}
				?>
				<div class="accordion-container">
					<div class="ac">
						<div class="header-container ac-header">
							<h2 class="form-header"><button type="button" class="ac-trigger">Account details</button></h2>
						</div>
						<div class="ac-panel">
							<div class="notices-container">
								<?php if ($show_notices): ?>
									<?php wc_print_notices(); ?>
								<?php endif; ?>
							</div>
							<form class="edit-account" action="" method="post">
								<div class="form-fields-container">
									<div class="form-fields-column">
										<div class="input-wrap">
										<input type="text" class="input-text" name="account_first_name"
													 id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>"/>
										</div>
										<div class="input-wrap">
										<input type="text" class="input-text" name="account_last_name"
													 id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>"/>
										</div>
										<div class="input-wrap">
										<input type="email" class="input-text" name="account_email"
													 id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>"/>
										</div>
									</div>
									<div class="form-fields-column">
										<div class="input-wrap">
										<input type="password" class="input-text" name="password_current" 
													 id="password_current" placeholder="Current password (leave blank to keep unchanged)" />
										</div>
										<div class="input-wrap">
										<input type="password" class="input-text" name="password_1" 
													 id="password_1" placeholder="New password (leave blank to keep unchanged)" />
										</div>
										<div class="input-wrap">
										<input type="password" class="input-text" name="password_2" 
													 id="password_2" placeholder="Confirm new password" />
										</div>
									</div>
								</div>
								<div class="btn-wrap" >
										<?php wp_nonce_field( 'save_account_details' ); ?>
										<input type="submit" class="btn" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"/>
										<input type="hidden" name="action" value="save_account_details"/>
								</div>
							</form>
						</div>
					</div>
				</div>
				<script>
					<?php if ($show_notices): ?>
						new Accordion('.accordion-container', { openOnInit: [0] } ); // show expanded accordion on page load 
					<?php else: ?>
						new Accordion('.accordion-container');
					<?php endif; ?>
				</script>
				
				<?php

				$form_html = ob_get_contents();
				ob_end_clean();

				$out = '<div class="account-details-wrapper"><div class="account-details-inner">' . $form_html . '</div></div>';
			}
			
		} 

		return $out;
	}
}

$edit_account_details_shortcode = new Account_Edit_Details_Form_Shortcode();



class Account_Billing_Form_Shortcode {
	 
	public function __construct() {
			add_shortcode('account_edit_billing_form', array($this, 'render_shortcode'));
	}

	/**
	 * based on WC_Shortcode_My_Account::edit_address()
	 *
	 */
	public static function prepare_address_fields( $user_id ) {		
		$current_user = wp_get_current_user();
		$load_address = 'billing';
		$country      = get_user_meta( $user_id, $load_address . '_country', true );

		if ( ! $country ) {
			$country = WC()->countries->get_base_country();
		}

		if ( 'billing' === $load_address ) {
			$allowed_countries = WC()->countries->get_allowed_countries();

			if ( ! array_key_exists( $country, $allowed_countries ) ) {
				$country = current( array_keys( $allowed_countries ) );
			}
		}

		$address = WC()->countries->get_address_fields( $country, $load_address . '_' );

		// Enqueue scripts.
		wp_enqueue_script( 'wc-country-select' );
		wp_enqueue_script( 'wc-address-i18n' );

		// Prepare values.
		foreach ( $address as $key => $field ) {

			$value = get_user_meta( $user_id, $key, true );

			if ( ! $value ) {
				switch ( $key ) {
					case 'billing_email':
					case 'shipping_email':
						$value = $current_user->user_email;
						break;
				}
			}

			$address[ $key ]['value'] = apply_filters( 'woocommerce_my_account_edit_address_field_value', $value, $key, $load_address );
		}

		return apply_filters( 'woocommerce_address_to_edit', $address, $load_address );
	}
	
	/**
	 * Outputs a billing form field.
	 * Based on woocommerce_form_field()
	 *
	 * @param string $key Key.
	 * @param mixed  $args Arguments.
	 * @param string $value (default: null).
	 * @return string
	 */
	private function woocommerce_form_field( $key, $args, $value = null ) {
		$defaults = array(
			'type'              => 'text',
			'label'             => '',
			'description'       => '',
			'placeholder'       => '',
			'maxlength'         => false,
			'required'          => false,
			'autocomplete'      => false,
			'id'                => $key,
			'class'             => array(),
			'label_class'       => array(),
			'input_class'       => array(),
			'return'            => false,
			'options'           => array(),
			'custom_attributes' => array(),
			'validate'          => array(),
			'default'           => '',
			'autofocus'         => '',
			'priority'          => '',
		);

		$args = wp_parse_args( $args, $defaults );
		$args = apply_filters( 'woocommerce_form_field_args', $args, $key, $value );
		
		if ( $args['placeholder'] == '') {
			$args['placeholder'] = $args['label'];
		}

		if ( $args['required'] ) {
			$args['class'][] = 'validate-required';
			$required = ' (required)';
		} else {
			$required = '';
		}

		if ( is_string( $args['label_class'] ) ) {
			$args['label_class'] = array( $args['label_class'] );
		}

		if ( is_null( $value ) ) {
			$value = $args['default'];
		}

		// Custom attribute handling.
		$custom_attributes         = array();
		$args['custom_attributes'] = array_filter( (array) $args['custom_attributes'], 'strlen' );

		if ( $args['maxlength'] ) {
			$args['custom_attributes']['maxlength'] = absint( $args['maxlength'] );
		}

		if ( ! empty( $args['autocomplete'] ) ) {
			$args['custom_attributes']['autocomplete'] = $args['autocomplete'];
		}

		if ( true === $args['autofocus'] ) {
			$args['custom_attributes']['autofocus'] = 'autofocus';
		}

		if ( $args['description'] ) {
			$args['custom_attributes']['aria-describedby'] = $args['id'] . '-description';
		}

		if ( ! empty( $args['custom_attributes'] ) && is_array( $args['custom_attributes'] ) ) {
			foreach ( $args['custom_attributes'] as $attribute => $attribute_value ) {
				$custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
			}
		}

		if ( ! empty( $args['validate'] ) ) {
			foreach ( $args['validate'] as $validate ) {
				$args['class'][] = 'validate-' . $validate;
			}
		}

		$field           = '';
		$label_id        = $args['id'];
		$sort            = $args['priority'] ? $args['priority'] : '';
		$field_container = '<p class="form-row %1$s" id="%2$s" data-priority="' . esc_attr( $sort ) . '">%3$s</p>';

		switch ( $args['type'] ) {
			case 'country':
				$countries = 'shipping_country' === $key ? WC()->countries->get_shipping_countries() : WC()->countries->get_allowed_countries();

				if ( 1 === count( $countries ) ) {

					$field .= '<strong>' . current( array_values( $countries ) ) . '</strong>';

					$field .= '<input type="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="' . current( array_keys( $countries ) ) . '" ' . implode( ' ', $custom_attributes ) . ' class="country_to_state" readonly="readonly" />';

				} else {

					$field = '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="country_to_state country_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . '><option value="">' . esc_html__( 'Select a country / region&hellip;', 'woocommerce' ) . '</option>';

					foreach ( $countries as $ckey => $cvalue ) {
						$field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . esc_html( $cvalue ) . '</option>';
					}

					$field .= '</select>';

					$field .= '<noscript><button type="submit" name="woocommerce_checkout_update_totals" value="' . esc_attr__( 'Update country / region', 'woocommerce' ) . '">' . esc_html__( 'Update country / region', 'woocommerce' ) . '</button></noscript>';

				}

				break;
			case 'state':
				/* Get country this state field is representing */
				$for_country = isset( $args['country'] ) ? $args['country'] : WC()->checkout->get_value( 'billing_state' === $key ? 'billing_country' : 'shipping_country' );
				$states      = WC()->countries->get_states( $for_country );

				if ( is_array( $states ) && empty( $states ) ) {

					$field_container = '<p class="form-row %1$s" id="%2$s" style="display: none">%3$s</p>';

					$field .= '<input type="hidden" class="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="" ' . implode( ' ', $custom_attributes ) . ' placeholder="' . esc_attr( $args['placeholder'] ) . '" readonly="readonly" data-input-classes="' . esc_attr( implode( ' ', $args['input_class'] ) ) . '"/>';

				} elseif ( ! is_null( $for_country ) && is_array( $states ) ) {

					$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="state_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ? $args['placeholder'] : esc_html__( 'Select an option&hellip;', 'woocommerce' ) ) . '"  data-input-classes="' . esc_attr( implode( ' ', $args['input_class'] ) ) . '">
						<option value="">' . esc_html__( 'Select an option&hellip;', 'woocommerce' ) . '</option>';

					foreach ( $states as $ckey => $cvalue ) {
						$field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . esc_html( $cvalue ) . '</option>';
					}

					$field .= '</select>';

				} else {

					$field .= '<input type="text" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $value ) . '"  placeholder="' . esc_attr( $args['placeholder'] ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" ' . implode( ' ', $custom_attributes ) . ' data-input-classes="' . esc_attr( implode( ' ', $args['input_class'] ) ) . '"/>';

				}

				break;
			case 'textarea':
				$field .= '<textarea name="' . esc_attr( $key ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '" ' . ( empty( $args['custom_attributes']['rows'] ) ? ' rows="2"' : '' ) . ( empty( $args['custom_attributes']['cols'] ) ? ' cols="5"' : '' ) . implode( ' ', $custom_attributes ) . '>' . esc_textarea( $value ) . '</textarea>';

				break;
			case 'checkbox':
				$field = '<label class="checkbox ' . implode( ' ', $args['label_class'] ) . '" ' . implode( ' ', $custom_attributes ) . '>
						<input type="' . esc_attr( $args['type'] ) . '" class="input-checkbox ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="1" ' . checked( $value, 1, false ) . ' /> ' . $args['label'] . $required . '</label>';

				break;
			case 'text':
			case 'password':
			case 'datetime':
			case 'datetime-local':
			case 'date':
			case 'month':
			case 'time':
			case 'week':
			case 'number':
			case 'email':
			case 'url':
			case 'tel':
				$field .= '<input type="' . esc_attr( $args['type'] ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '"  value="' . esc_attr( $value ) . '" ' . implode( ' ', $custom_attributes ) . ' />';

				break;
			case 'select':
				$field   = '';
				$options = '';

				if ( ! empty( $args['options'] ) ) {
					foreach ( $args['options'] as $option_key => $option_text ) {
						if ( '' === $option_key ) {
							// If we have a blank option, select2 needs a placeholder.
							if ( empty( $args['placeholder'] ) ) {
								$args['placeholder'] = $option_text ? $option_text : __( 'Choose an option', 'woocommerce' );
							}
							$custom_attributes[] = 'data-allow_clear="true"';
						}
						$options .= '<option value="' . esc_attr( $option_key ) . '" ' . selected( $value, $option_key, false ) . '>' . esc_html( $option_text ) . '</option>';
					}

					$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">
							' . $options . '
						</select>';
				}

				break;
			case 'radio':
				$label_id .= '_' . current( array_keys( $args['options'] ) );

				if ( ! empty( $args['options'] ) ) {
					foreach ( $args['options'] as $option_key => $option_text ) {
						$field .= '<input type="radio" class="input-radio ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $option_key ) . '" name="' . esc_attr( $key ) . '" ' . implode( ' ', $custom_attributes ) . ' id="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '"' . checked( $value, $option_key, false ) . ' />';
						$field .= '<label for="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '" class="radio ' . implode( ' ', $args['label_class'] ) . '">' . esc_html( $option_text ) . '</label>';
					}
				}

				break;
		}
		return $field;
	}
			
	private function rearrange_fields( $fields ) {
		
		$sorted_fields = array();
		
		$target_order = array(
			// first column	
				'billing_first_name',
				'billing_company',
				'billing_state', // county inside country
				'billing_address_1',
				'billing_phone',
				
			// second column
				'billing_last_name',
				'billing_country', // country/region
				'billing_city',
				'billing_postcode',
				'billing_email'
		);
		
		foreach ( $target_order as $field_name ) {
			foreach ( $fields as $key => $field ) {
				if ( $field_name == $key ) {
					$sorted_fields[$field_name] = $field;
				}
			}
		}
		
		return $sorted_fields;
	}
	
	public function render_shortcode() {
		$user_id = get_current_user_id();
		$out = '';

		if ( $user_id ) {
			$user = get_user_by( 'id', get_current_user_id());
			
			if ( $user ) {
				
				$address_fields = $this->rearrange_fields(
					self::prepare_address_fields( $user_id )
				);
				
				$show_notices = ( isset( $_POST['action']) && ( $_POST['action'] == 'edit_address') );
				
				ob_start();
				$counter = 0;
				$fields_in_column = ceil( count($address_fields) / 2 );
				?>
				<div class="accordion-container-billing">
					<div class="ac is-active">
						<div class="header-container ac-header">
							<h2 class="form-header"><button type="button" class="ac-trigger">Billing details</button></h2>
						</div>
						<div class="ac-panel">
							<div class="notices-container">
								<?php if ( $show_notices ): ?>
									<?php wc_print_notices(); ?>
								<?php endif; ?>
							</div>
							<form class="edit-billing" action="" method="post">
								<div class="form-fields-container">
									<?php	foreach ( $address_fields as $key => $field ) : ?>
									
										<?php if ( ($counter % $fields_in_column) == 0): ?>
											<div class="form-fields-column">
										<?php endif; ?>
												
										<div class="input-wrap">
											<?php echo $this->woocommerce_form_field( $key, $field, wc_get_post_data_by_key( $key, $field['value'] ) ); ?>
										</div>
												
										<?php if ( ( ($counter+1) % $fields_in_column) == 0 || $counter == count($address_fields) - 1 ): ?>
											</div>
										<?php endif; ?>
												
										<?php $counter++; ?>
												
									<?php endforeach; ?>
								</div>
								<div class="btn-wrap" >
										<?php wp_nonce_field( 'woocommerce-edit_address', 'woocommerce-edit-address-nonce' ); ?>
										<input type="submit" class="btn" name="save_address" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"/>
										<input type="hidden" name="action" value="edit_address"/>
								</div>
							</form>
						</div>
					</div>
				</div>
				<script>
					<?php if ($show_notices): ?>
						new Accordion('.accordion-container-billing', { openOnInit: [0] } ); // show expanded accordion on page load 
					<?php else: ?>
						new Accordion('.accordion-container-billing');
					<?php endif; ?>
				</script>
				<?php

				$form_html = ob_get_contents();
				ob_end_clean();

				$out = '<div class="billing-details-wrapper"><div class="billing-details-inner">' . $form_html . '</div></div>';
			}
			
		} 

		return $out;
	}
}

$edit_billing_details_shortcode = new Account_Billing_Form_Shortcode();